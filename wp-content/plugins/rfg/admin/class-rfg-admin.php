<?php

/**
 * The admin-specific functionality of the plugin.
 *
 * @since      1.0.0
 *
 * @package    Rfg
 * @subpackage Rfg/admin
 */

/**
 * The admin-specific functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    Rfg
 * @subpackage Rfg/admin
 * @author     Oranginalab
 */

define( 'RFG_ADMIN_UPDATE_POST_TITLES', true );

class Rfg_Admin {
	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string $rfg The ID of this plugin.
	 */
	private $rfg;
	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string $version The current version of this plugin.
	 */
	private $version;
	/**
	 * @var     array $options This plugin options object used when dealing with the OptionsAPI
	 */
	private $options;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @param   string  $rfg      The name of this plugin.
	 * @param   string  $version  The version of this plugin.
	 *
	 * @since    1.0.0
	 */
	public function __construct( $rfg, $version ) {
		$this->rfg     = $rfg;
		$this->version = $version;

		// Retrieve plugin options to be used by all methods using OptionsAPI
		$this->options = get_option( $this->rfg . '_options' );

		$this->define_rest_routes();

		// Reserved to add plugin options in Admin.
		add_action( 'admin_menu', [ $this, 'addAdminMenu' ] );
		add_action( 'admin_init', [ $this, 'settingsInit' ] );
	}

	/**
	 * Register the stylesheets for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles() {
		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Rfg_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Rfg_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_style( $this->rfg, plugin_dir_url( __FILE__ ) . 'css/rfg-admin.css', [], $this->version, 'all' );
	}

	/**
	 * Register the JavaScript for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts() {
		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Rfg_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Rfg_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_script( $this->rfg, plugin_dir_url( dirname( __FILE__ ) ) . 'assets/js/admin.js', [], $this->version, false );

		wp_localize_script( $this->rfg, 'wpRfgApiSettings', [
			'root'  => esc_url_raw( rest_url() . $this->rfg . '/v' . intval( $this->version ) . '/' ),
			'nonce' => wp_create_nonce( 'wp_rest' ),
		] );
	}

	public function addAdminMenu() {
		add_options_page( 'Rfg', 'Rfg', 'manage_options', $this->rfg, [
			$this,
			'optionsPage',
		] );
	}

	public function settingsInit() {
		register_setting( 'rfg', $this->rfg . '_settings' );

		add_settings_section(
			'rfg_sharing_section',
			__( 'Social Sharing Metas', 'refugis' ),
			[ $this, 'settingsSectionCallback' ],
			'rfg'
		);

		add_settings_field(
			'rfg_twitter_site_meta',
			__( 'Twitter site meta', 'refugis' ),
			[ $this, 'settingsRenderCallback' ],
			'rfg',
			'rfg_sharing_section',
			[ 'field' => 'rfg_twitter_site_meta' ]
		);

		add_settings_field(
			'rfg_twitter_creator_meta',
			__( 'Twitter creator meta', 'refugis' ),
			[ $this, 'settingsRenderCallback' ],
			'rfg',
			'rfg_sharing_section',
			[ 'field' => 'rfg_twitter_creator_meta' ]
		);
	}

	public function settingsSectionCallback() {
		echo __( 'Social sharing settings', 'refugis' );
	}

	public function settingsRenderCallback( $args ) {
		$options = get_option( $this->rfg . '_settings', [] );

		$options = array_merge( [
			'rfg_twitter_site_meta'    => '',
			'rfg_twitter_creator_meta' => '',
		], $options );

		$field = $args['field'];

		include dirname( __FILE__ ) . '/partials/rfg-admin-settings-display.php';
	}

	public function optionsPage() {
		include dirname( __FILE__ ) . '/partials/rfg-admin-options-page.php';
	}

	public function addScanSheltersButton() {
		global $current_screen;

		if ( RFG_SHELTER_POST_TYPE != $current_screen->post_type ) {
			return;
		}

		?>
        <script>
          jQuery(function () {
            jQuery("body.post-type-<?php echo RFG_SHELTER_POST_TYPE?> .wrap h1").append('<button id="rfg-scan-shelters" class="page-title-action">Scan Shelters ...</button>');
          });
        </script>
		<?php
	}

	private function define_rest_routes() {
		add_action( 'rest_api_init', function () {
			// Allow more headers: make it compatible with http and https.
			add_filter( 'rest_pre_serve_request', function ( $value ) {
				header( 'Access-Control-Allow-Headers: Authorization, X-WP-Nonce, X-CSRF-TOKEN, Content-Type, X-Requested-With' );
			} );
			register_rest_route( $this->rfg . '/v' . intval( $this->version ), '/scan-shelters', [
				'methods'  => 'POST',
				'callback' => [ $this, 'scanShelters' ],
			] );
		} );
	}

	public function scanShelters( WP_REST_Request $request ) {
		wp_verify_nonce( $request->get_header( 'X-WP-Nonce' ), 'wp_rest' );

		if ( ! current_user_can( 'edit_posts' ) ) {
			die( 'Unauthorized' );
		}

		$endpoint = RFG_SHELTERS_API_BASE_URL . 'refugis';

		$args = [
			'timeout' => 30,
		];

		// Do remote get
		$data     = [
			'success' => true,
		];
		$code     = 0;
		$response = wp_remote_get( $endpoint, $args );

		if ( ! is_wp_error( $response ) && ( $code = wp_remote_retrieve_response_code( $response ) ) == 200 ) {
			$data['json'] = json_decode( wp_remote_retrieve_body( $response ) );
		} else {
			$data['success'] = false;
			$data['error']   = is_wp_error( $response ) ? $response->get_error_message() : $code;

			return new WP_REST_Response( [
				'status' => 'NOK',
				'error'  => $data['error'],
			] );
		}

		$shelters = $data['json']->refugis;

		$existing   = [];
		$deprecated = 0;
		$inserted   = 0;
		$updated    = 0;

		// Required to set language details for inserted posts.
		global $sitepress;

		foreach ( $shelters as $shelter ) {
			$existing[ $shelter->CaseId ] = $shelter->Name;
		}

		$posts = get_posts( [
			'post_type'      => RFG_SHELTER_POST_TYPE,
			'post_status'    => 'all',
			'posts_per_page' => - 1,
		] );

		wp_queue_posts_for_term_meta_lazyload( $posts );

		// TODO: we can update and insert posts in chunks...

		// Can be a lengthy operation.
		set_time_limit( 300 );

		foreach ( $posts as $post ) {
			$shelter_api_id = get_post_meta( $post->ID, RFG_SHELTER_API_ID_META_KEY, true );
			if ( ! array_key_exists( $shelter_api_id, $existing ) ) {
				// Deprecated.
				wp_update_post( [
					'ID'          => $post->ID,
					'post_status' => 'trash',
				] );
				$deprecated ++;
			} else {
				if ( RFG_ADMIN_UPDATE_POST_TITLES && $post->post_title != $existing[ $shelter_api_id ] ) {
					// Update post title.
					wp_update_post( [
						'ID'         => $post->ID,
						'post_title' => $existing[ $shelter_api_id ],
						'post_name'  => sanitize_title( $existing[ $shelter_api_id ] ),
					] );
					$updated ++;
				}

				// *** RESERVED ***
				// $this->populateTranslatedTaxonomies( $post->ID, RFG_SHELTER_GROUP_TAXONOMY );
				// $this->populateTranslatedTaxonomies( $post->ID, RFG_SHELTER_CATEGORY_TAXONOMY );

				unset( $existing[ $shelter_api_id ] );
			}
		}

		foreach ( $existing as $shelter_api_id => $title ) {
			$post_id = wp_insert_post( [
				'post_type'    => RFG_SHELTER_POST_TYPE,
				'post_status'  => 'draft',
				'post_title'   => $title,
				'post_name'    => sanitize_title( $title ),
				'post_content' => "Shelter post content goes here...",
				'meta_input'   => [
					RFG_SHELTER_API_ID_META_KEY             => $shelter_api_id,
					// Default values here.
					RFG_SHELTER_DESCRIPTION_META_KEY        => "Shelter post content goes here...",
					RFG_SHELTER_VISITABLE_META_KEY          => true,
					RFG_SHELTER_CONSERVATION_STATE_META_KEY => "good",
				],
			] );

			if ( is_wp_error( $post_id ) ) {
				return new WP_REST_Response( [
					'status' => 'NOK',
					'error'  => $post_id->get_error_message(),
				] );
			}

			if ( isset( $sitepress ) ) {
				// We must enable WPML details for recently created post.
				$def_lang = $sitepress->get_default_language();
				if ( $def_lang ) {
					$sitepress->set_element_language_details( $post_id, 'post_' . RFG_SHELTER_POST_TYPE, false, $def_lang );
				}
			}

			$inserted ++;
		}

		return new WP_REST_Response( [
			'status'     => 'OK',
			'inserted'   => $inserted,
			'updated'    => $updated,
			'deprecated' => $deprecated,
		] );
	}

	private function populateTranslatedTaxonomies( $post_id, $taxonomy ) {
		$supported_languages = array_keys( apply_filters( 'wpml_active_languages', null ) );
		$post_terms          = get_the_terms( $post_id, $taxonomy );
		if ( $post_terms ) {
			// get_the_terms() returns duplicates. Here we need unique terms.
			$post_terms = array_unique( $post_terms, SORT_REGULAR );
			$taxonomies = [];
			foreach ( $post_terms as $term ) {
				foreach ( $supported_languages as $code ) {
					$taxonomies[] = apply_filters( 'wpml_object_id', $term->term_id, $taxonomy, false, $code );
				}
			}
			$taxonomies = array_filter( $taxonomies );
			// Replace terms with translated terms set.
			if ( ! empty( $taxonomies ) ) {
				wp_set_post_terms( $post_id, $taxonomies, $taxonomy );
			}
		}
	}
}
