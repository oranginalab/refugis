<?php

/**
 * Fired during plugin activation
 *
 * @since      1.0.0
 *
 * @package    Rfg
 * @subpackage Rfg/includes
 */

/**
 * Fired during plugin activation.
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @since      1.0.0
 * @package    Rfg
 * @subpackage Rfg/includes
 * @author     Oranginalab
 */
class Rfg_Activator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function activate() {

	}

}
