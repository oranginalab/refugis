<?php

/**
 * The file that defines the core plugin class
 *
 * A class definition that includes attributes and functions used across both the
 * public-facing side of the site and the admin area.
 *
 * @since      1.0.0
 *
 * @package    Rfg
 * @subpackage Rfg/includes
 */

// Custom post type and custom fields settings.
// *** Make sure we get them in sync with WP plugins configuration! ***
define( 'RFG_SHELTER_POST_TYPE', 'refugis' );

define( 'RFG_SHELTER_API_ID_META_KEY', 'api_id' );
define( 'RFG_SHELTER_DESCRIPTION_META_KEY', 'description');
define( 'RFG_SHELTER_CONSERVATION_STATE_META_KEY', 'conservation_state');
define( 'RFG_SHELTER_VISITABLE_META_KEY', 'visitable');

define( 'RFG_SHELTER_VIDEO_TESTIMONIAL_META_KEY', 'video_testimoni');
define( 'RFG_SHELTER_VIDEO_TESTIMONIAL_PEU_META_KEY', 'peu_video_testimoni');
define( 'RFG_SHELTER_VIDEO_TOUR_META_KEY', 'tour_virtual');
define( 'RFG_SHELTER_VIDEO_TOUR_PEU_META_KEY', 'peu_tour_virtual');
define( 'RFG_SHELTER_ID_CARTA_META_KEY', 'id_carta_arqueologica');
define( 'RFG_SHELTER_PLANIMETRIA_META_KEY', 'planimetria');


define( 'RFG_SHELTER_CATEGORY_TAXONOMY', 'categoria_refugis');
define( 'RFG_SHELTER_GROUP_TAXONOMY', 'agrupacio');

define( 'RFG_SHELTER_LIST_API_SHELTERS_PROP', 'refugis' );
define( 'RFG_SHELTER_LIST_API_SHELTER_API_ID_PROP', 'CaseId');

// Shelters API URL.

// Staging API
//define( 'RFG_SHELTERS_API_BASE_URL', 'http://35.187.1.125:8082/api/' );
//define( 'RFG_SHELTERS_DOC_BASE_URL', 'http://35.187.1.125:8082' ); // Added for consistency with production environment. Not sure there are any docs stored here.

// Production API
define( 'RFG_SHELTERS_API_BASE_URL', 'http://51.75.130.149:8082/api/' );
define( 'RFG_SHELTERS_DOC_BASE_URL', 'http://51.75.130.149:8082' );

/**
 * The core plugin class.
 *
 * This is used to define internationalization, admin-specific hooks, and
 * public-facing site hooks.
 *
 * Also maintains the unique identifier of this plugin as well as the current
 * version of the plugin.
 *
 * @since      1.0.0
 * @package    Rfg
 * @subpackage Rfg/includes
 * @author     Oranginalab
 */
class Rfg {
	/**
	 * The loader that's responsible for maintaining and registering all hooks that power
	 * the plugin.
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      Rfg_Loader $loader Maintains and registers all hooks for the plugin.
	 */
	protected $loader;
	/**
	 * The unique identifier of this plugin.
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      string $rfg The string used to uniquely identify this plugin.
	 */
	protected $rfg;
	/**
	 * The current version of the plugin.
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      string $version The current version of the plugin.
	 */
	protected $version;

	/**
	 * Define the core functionality of the plugin.
	 *
	 * Set the plugin name and the plugin version that can be used throughout the plugin.
	 * Load the dependencies, define the locale, and set the hooks for the admin area and
	 * the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function __construct() {
		if ( defined( 'RFG_VERSION' ) ) {
			$this->version = RFG_VERSION;
		} else {
			$this->version = '1.0.0';
		}
		$this->rfg = 'rfg';

		$this->load_dependencies();
		$this->set_locale();
		$this->define_admin_hooks();
		$this->define_public_hooks();
	}

	/**
	 * Load the required dependencies for this plugin.
	 *
	 * Include the following files that make up the plugin:
	 *
	 * - Rfg_Loader. Orchestrates the hooks of the plugin.
	 * - Rfg_i18n. Defines internationalization functionality.
	 * - Rfg_Admin. Defines all hooks for the admin area.
	 * - Rfg_Public. Defines all hooks for the public side of the site.
	 *
	 * Create an instance of the loader which will be used to register the hooks
	 * with WordPress.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function load_dependencies() {
		/**
		 * The class responsible for orchestrating the actions and filters of the
		 * core plugin.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-rfg-loader.php';

		/**
		 * The class responsible for defining internationalization functionality
		 * of the plugin.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-rfg-i18n.php';

		/**
		 * The class responsible for defining all actions that occur in the admin area.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'admin/class-rfg-admin.php';

		/**
		 * The class responsible for defining all actions that occur in the public-facing
		 * side of the site.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'public/class-rfg-public.php';

		$this->loader = new Rfg_Loader();
	}

	/**
	 * Define the locale for this plugin for internationalization.
	 *
	 * Uses the Rfg_i18n class in order to set the domain and to register the hook
	 * with WordPress.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function set_locale() {
		$plugin_i18n = new Rfg_i18n();

		$this->loader->add_action( 'plugins_loaded', $plugin_i18n, 'load_plugin_textdomain' );
	}

	/**
	 * Register all of the hooks related to the admin area functionality
	 * of the plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function define_admin_hooks() {
		$plugin_admin = new Rfg_Admin( $this->get_rfg(), $this->get_version() );

		$this->loader->add_action( 'admin_enqueue_scripts', $plugin_admin, 'enqueue_styles' );
		$this->loader->add_action( 'admin_enqueue_scripts', $plugin_admin, 'enqueue_scripts' );

		$this->loader->add_action( 'admin_head-edit.php', $plugin_admin, 'addScanSheltersButton' );
	}

	/**
	 * Register all of the hooks related to the public-facing functionality
	 * of the plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function define_public_hooks() {
		$plugin_public = new Rfg_Public( $this->get_rfg(), $this->get_version() );

		$this->loader->add_action( 'wp_enqueue_scripts', $plugin_public, 'enqueue_styles' );
		$this->loader->add_action( 'wp_enqueue_scripts', $plugin_public, 'enqueue_scripts' );
	}

	/**
	 * Run the loader to execute all of the hooks with WordPress.
	 *
	 * @since    1.0.0
	 */
	public function run() {
		$this->loader->run();
	}

	/**
	 * The name of the plugin used to uniquely identify it within the context of
	 * WordPress and to define internationalization functionality.
	 *
	 * @return    string    The name of the plugin.
	 * @since     1.0.0
	 */
	public function get_rfg() {
		return $this->rfg;
	}

	/**
	 * The reference to the class that orchestrates the hooks with the plugin.
	 *
	 * @return    Rfg_Loader    Orchestrates the hooks of the plugin.
	 * @since     1.0.0
	 */
	public function get_loader() {
		return $this->loader;
	}

	/**
	 * Retrieve the version number of the plugin.
	 *
	 * @return    string    The version number of the plugin.
	 * @since     1.0.0
	 */
	public function get_version() {
		return $this->version;
	}
}
