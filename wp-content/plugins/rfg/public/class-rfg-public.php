<?php

/**
 * The public-facing functionality of the plugin.
 *
 * @since      1.0.0
 *
 * @package    Rfg
 * @subpackage Rfg/public
 */

/**
 * The public-facing functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the public-facing stylesheet and JavaScript.
 *
 * @package    Rfg
 * @subpackage Rfg/public
 * @author     Oranginalab
 */
class Rfg_Public {
	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string $rfg The ID of this plugin.
	 */
	private $rfg;
	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string $version The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @param   string  $rfg      The name of the plugin.
	 * @param   string  $version  The version of this plugin.
	 *
	 * @since    1.0.0
	 */
	public function __construct( $rfg, $version ) {
		$this->rfg     = $rfg;
		$this->version = $version;

		// Register shortcode.
		$this->register_shortcode();

		// REST routes.
		$this->define_rest_routes();
	}

	/**
	 * Register the stylesheets for the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles() {
		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Rfg_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Rfg_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_style( $this->rfg, plugin_dir_url( __FILE__ ) . 'css/rfg-public.css', [], $this->version, 'all' );
	}

	/**
	 * Register the JavaScript for the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts() {
		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Rfg_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Rfg_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		// Shelter styles.
		wp_enqueue_script( $this->rfg . '-shelter-styles', get_template_directory_uri() . '/js/shelter-styles.js', [], $this->version, true );

		// Main script (webpack)
		wp_enqueue_script( $this->rfg, plugin_dir_url( dirname( __FILE__ ) ) . 'assets/js/frontend.js', [], $this->version, true );

		wp_localize_script( $this->rfg, 'wpRfgApiSettings', [
			'root'    => esc_url_raw( rest_url() . $this->rfg . '/v' . intval( $this->version ) . '/' ),
			'nonce'   => wp_create_nonce( 'wp_rest' ),
			// We are prepared for WPML plugin.
			// *** IMPORTANT: WPML plugin must be configured to use URL segment for language code!
			'lang'    => defined( 'ICL_LANGUAGE_CODE' ) ? ICL_LANGUAGE_CODE : '',
			'strings' => [
				// Here we can define some translated strings for frontend.
				'All' => __( 'Totes', 'refugis' ),
				// More translations go here...
			],

		] );

		wp_localize_script( $this->rfg, 'wpUrl', [
			'base' => rtrim( home_url(), '/' ) . '/',
		] );
	}

	/**
	 * Plugin shortcodes.
	 */
	public function register_shortcode() {
		add_shortcode( 'rfg', [ $this, 'render_rfg' ] );
		add_shortcode( 'shelter_link', [ $this, 'render_shelter_link' ] );
	}

	/**
	 * @param   array  $attrs
	 *
	 * @return false|string
	 */
	public function render_rfg( $attrs = [] /*, $content = null */ ) {
		$attrs = shortcode_atts( [
			// Default attrs here...
		], $attrs );

		ob_start();

		include dirname( __FILE__ ) . '/partials/rfg-public-display.php';

		return ob_get_clean();
	}

	public function render_shelter_link( $attrs = [] /*, $content = null */ ) {
		$attrs = shortcode_atts( [
			'id'   => null,
			'text' => __( 'View shelter details', 'refugis' ),
		], $attrs );

		ob_start();

		include dirname( __FILE__ ) . '/partials/rfg-shelter-link.php';

		return ob_get_clean();
	}

	private function define_rest_routes() {
		add_action( 'rest_api_init', function () {
			// Allow more headers: make it compatible with http and https.
			add_filter( 'rest_pre_serve_request', function ( $value ) {
				header( 'Access-Control-Allow-Headers: Authorization, X-WP-Nonce, X-CSRF-TOKEN, Content-Type, X-Requested-With' );
			} );
			register_rest_route( $this->rfg . '/v' . intval( $this->version ), '/shelter/(?P<id>\d+)', [
				'methods'  => 'GET',
				'callback' => [ $this, 'getShelter' ],
			] );
			register_rest_route( $this->rfg . '/v' . intval( $this->version ), '/shelters', [
				'methods'  => 'POST',
				'callback' => [ $this, 'getSheltersList' ],
			] );
			register_rest_route( $this->rfg . '/v' . intval( $this->version ), '/districts', [
				'methods'  => 'GET',
				'callback' => [ $this, 'getDistrictsList' ],
			] );
			register_rest_route( $this->rfg . '/v' . intval( $this->version ), '/categories', [
				'methods'  => 'GET',
				'callback' => [ $this, 'getCategoriesList' ],
			] );

			register_rest_route( $this->rfg . '/v' . intval( $this->version ), '/shelter-html/(?P<id>\d+)', [
				'methods'  => 'GET',
				'callback' => [ $this, 'getShelterHtml' ],
			] );
		} );
	}

	// Testing cURL calls to shelter API.

	// Shelter data by ID.
	public function getShelter( WP_REST_Request $request ) {
		$id = $request->get_param( 'id' );

		$endpoint = RFG_SHELTERS_API_BASE_URL . 'refugi/' . $id;

		$api_data = $this->doRequest( $endpoint );

		if ( ! $api_data['success'] ) {
			return $api_data;
		}

		// Get WP post by shelter ID (api_id custom field).
		$args = [
			'numberposts' => 1,
			'post_type'   => RFG_SHELTER_POST_TYPE,
			'meta_key'    => RFG_SHELTER_API_ID_META_KEY,
			'meta_value'  => $id,
			// In production we should filter by 'post_status' => 'publish'
			'post_status' => defined( 'WP_DEBUG' ) && true === WP_DEBUG ? 'all' : 'publish',
		];

		$posts = get_posts( $args );

		if ( ! empty( $posts ) ) {
			$post = $posts[0];

			// Retrieve custom field values.
			// We rely on WP optimization and presume that no additional SQL queries are performed
			// to get meta values.
			$meta = [
				RFG_SHELTER_DESCRIPTION_META_KEY        => get_post_meta( $post->ID, RFG_SHELTER_DESCRIPTION_META_KEY, true ),
				RFG_SHELTER_VISITABLE_META_KEY          => get_post_meta( $post->ID, RFG_SHELTER_VISITABLE_META_KEY, true ),
				RFG_SHELTER_CONSERVATION_STATE_META_KEY => get_post_meta( $post->ID, RFG_SHELTER_CONSERVATION_STATE_META_KEY, true ),
				RFG_SHELTER_API_ID_META_KEY             => get_post_meta( $post->ID, RFG_SHELTER_API_ID_META_KEY, true ),

				RFG_SHELTER_VIDEO_TESTIMONIAL_META_KEY     => get_post_meta( $post->ID, RFG_SHELTER_VIDEO_TESTIMONIAL_META_KEY, true ),
				RFG_SHELTER_VIDEO_TESTIMONIAL_PEU_META_KEY => get_post_meta( $post->ID, RFG_SHELTER_VIDEO_TESTIMONIAL_PEU_META_KEY, true ),
				RFG_SHELTER_VIDEO_TOUR_META_KEY            => get_post_meta( $post->ID, RFG_SHELTER_VIDEO_TOUR_META_KEY, true ),
				RFG_SHELTER_VIDEO_TOUR_PEU_META_KEY        => get_post_meta( $post->ID, RFG_SHELTER_VIDEO_TOUR_PEU_META_KEY, true ),
				RFG_SHELTER_ID_CARTA_META_KEY              => get_post_meta( $post->ID, RFG_SHELTER_ID_CARTA_META_KEY, true ),
				RFG_SHELTER_PLANIMETRIA_META_KEY           => get_post_meta( $post->ID, RFG_SHELTER_PLANIMETRIA_META_KEY, true ),
			];

			$post->meta                = $meta;
			$api_data['json']->wp_post = $post;
		}

		return $api_data;
	}

	public function getShelterHtml( WP_REST_Request $request ) {
		$response = $this->getShelter( $request );
		if ( $response['success'] ) {
			$api_data = $response['json'];
			$details  = $api_data->data->{$api_data->name};

			ob_start();

			include( locate_template( 'partials/rfg/shelter-html.php', false, false ) );

			$html = ob_get_clean();

			return [
				'success' => true,
				'html'    => $html,
				// Add native response data, just in case we need it in frontend.
				'data'    => $api_data,
			];
		} else {
			return [
				'success' => false,
				'error'   => $response['error'],
			];
		}
	}

	private static $filters = [
		'api'      => [
			'district'   => 'Districte',
			// 'address' => 'Adreca', // Strict address search.
			'address'    => 'Cerca', // Free-text location search, i.e. address, district, etc.
			'chronology' => 'CronologiaInicial',
		],
		'meta'     => [
			'visitable'          => RFG_SHELTER_VISITABLE_META_KEY,
			'conservation_state' => RFG_SHELTER_CONSERVATION_STATE_META_KEY,
		],
		'taxonomy' => [
			'category' => [
				'wp_property'   => 'categories',
				'taxonomy_name' => RFG_SHELTER_CATEGORY_TAXONOMY,
			],
			'group'    => [
				'wp_property'   => 'groups',
				'taxonomy_name' => RFG_SHELTER_GROUP_TAXONOMY,
			],
		],
	];

	// Shelters list.
	public function getSheltersList( WP_REST_Request $request ) {
		$endpoint = RFG_SHELTERS_API_BASE_URL . 'refugis';

		$params = $request->get_json_params();

		$filters = $this->mapFilters( $params['filters'] );

		// Special case - we have to use cURL here to send raw json in GET BODY.
		// Later we may refactor to use default WP remote get with right arguments and keep filters working.
		$client = new WP_Http_Curl;

		$response = $client->request( $endpoint, [
			'method'     => 'GET',
			'body'       => ! empty( $filters['api'] ) ? wp_json_encode( $filters['api'] ) : null,
			'timeout'    => 20,
			// Some defaults to avoid PHP warnings.
			'headers'    => [ 'user-agent' => 'RFG Plugin for WordPress' ],
			'stream'     => null,
			'filename'   => null,
			'decompress' => null,
		] );

		$api_data = [
			'success' => true,
		];
		$code     = 0;

		if ( ! is_wp_error( $response ) && ( $code = wp_remote_retrieve_response_code( $response ) ) == 200 ) {
			$api_data['json'] = json_decode( wp_remote_retrieve_body( $response ) );
		} else {
			$api_data['success'] = false;
			$api_data['error']   = is_wp_error( $response ) ? $response->get_error_message() : $code;

			return $api_data;
		}

		$shelters = $api_data['json']->{RFG_SHELTER_LIST_API_SHELTERS_PROP};

		// Get related WP posts - we need some meta data (custom fields) for further filtering.
		$args = [
			'numberposts'      => - 1,
			'post_type'        => RFG_SHELTER_POST_TYPE,
			// In production we should filter by 'post_status' => 'publish'
			'post_status'      => defined( 'WP_DEBUG' ) && true === WP_DEBUG ? 'all' : 'publish',
			// Only query posts related to current shelters API response.
			'meta_query'       => [
				'existing' => [
					'key'     => RFG_SHELTER_API_ID_META_KEY,
					'value'   => array_column( $shelters, RFG_SHELTER_LIST_API_SHELTER_API_ID_PROP ),
					'compare' => 'IN',
				],
			],
			'suppress_filters' => true,
		];

		// Workaround - make "suppress_filters" work.
		global $sitepress;
		remove_filter( 'get_terms_args', [ $sitepress, 'get_terms_args_filter' ], 10 );
		remove_filter( 'get_term', [ $sitepress, 'get_term_adjust_id' ], 1 );
		remove_filter( 'terms_clauses', [ $sitepress, 'terms_clauses' ], 10 );

		$posts = get_posts( $args );

		add_filter( 'get_terms_args', [ $sitepress, 'get_terms_args_filter' ], 10, 2 );
		add_filter( 'get_term', [ $sitepress, 'get_term_adjust_id' ], 1, 1 );
		add_filter( 'terms_clauses', [ $sitepress, 'terms_clauses' ], 10, 4 );
		// End workaround.

		wp_queue_posts_for_term_meta_lazyload( $posts );

		$filters_rules = self::$filters;

		$wp_data = array_map( function ( $post ) use ( $filters_rules ) {
			$api_id = get_post_meta( $post->ID, RFG_SHELTER_API_ID_META_KEY, true );

			$meta_values = [];
			foreach ( $filters_rules['meta'] as $key => $meta_key ) {
				$meta_values[ $key ] = get_post_meta( $post->ID, $meta_key, true );
			}

			foreach ( $filters_rules['taxonomy'] as $tax_keys ) {
				$meta_values[ $tax_keys['wp_property'] ] = get_the_terms( $post->ID, $tax_keys['taxonomy_name'] );

				if ( $meta_values[ $tax_keys['wp_property'] ] ) {
					$meta_values[ $tax_keys['wp_property'] ] = array_column( (array) $meta_values[ $tax_keys['wp_property'] ], 'name' );
				}
			}

			// Return required data enriched with meta values (taken both from meta and taxonomies).
			return array_merge( [
				'post_id'                   => $post->ID,
				RFG_SHELTER_API_ID_META_KEY => $api_id,
			], $meta_values );
		}, $posts );

		// Key posts data by api_id for faster access.
		$wp_data = array_combine( array_column( $wp_data, RFG_SHELTER_API_ID_META_KEY ), $wp_data );

		// Inject post data into resulting shelter dataset.
		$shelters = array_map( function ( $shelter ) use ( $wp_data ) {
			if ( isset( $wp_data[ $shelter->{RFG_SHELTER_LIST_API_SHELTER_API_ID_PROP} ] ) ) {
				$shelter->WpPost = $wp_data[ $shelter->{RFG_SHELTER_LIST_API_SHELTER_API_ID_PROP} ];
			}

			return $shelter;
		}, $shelters );

		// Do post meta and taxonomy filtering.
		if ( ! empty( $filters['meta'] ) || ! empty( $filters['taxonomy'] ) ) {
			// At this point we have shelter data enriched with WP posts data.
			$meta_filters     = $filters['meta'];
			$taxonomy_filters = $filters['taxonomy'];

			$filters_rules = self::$filters;

			$shelters = array_filter( $shelters, function ( $shelter ) use (
				$meta_filters,
				$taxonomy_filters,
				$filters_rules
			) {
				// Meta - just check single value.
				foreach ( $filters_rules['meta'] as $name => $prop_name ) {
					if ( isset( $meta_filters[ $name ] ) && ( ! isset( $shelter->WpPost[ $prop_name ] ) || $shelter->WpPost[ $prop_name ] != $meta_filters[ $name ] ) ) {
						return false;
					}
				}
				// Taxonomy - we support multiple values, just check that given filter value is in array.
				foreach ( $filters_rules['taxonomy'] as $name => $tax_keys ) {
					if ( isset( $taxonomy_filters[ $name ] ) && $taxonomy_filters[ $name ] ) {
						if ( ! isset( $shelter->WpPost[ $tax_keys['wp_property'] ] )
						     || ! is_array( $shelter->WpPost[ $tax_keys['wp_property'] ] )
						     || ! in_array( $taxonomy_filters[ $name ], $shelter->WpPost[ $tax_keys['wp_property'] ] ) ) {
							return false;
						}
					}
				}

				return true;
			} );
		}

		$shelters = array_values( $this->validateShelters( $shelters ) );

		// We treat empty result set as an error.
		if ( $shelters ) {
			$api_data['json']->{RFG_SHELTER_LIST_API_SHELTERS_PROP} = $shelters;
		} else {
			$api_data['success'] = false;
			$api_data['error']   = __( 'No s\'han trobat resultats coincidents amb la vostra cerca. Torneu-ho a provar.', 'refugis' );
		}

		return $api_data;
	}

	private function validateShelters( $shelters ) {
		// Discard items with empty coordinates.
		$invalid = [];

		$valid = array_filter( $shelters, function ( $item ) use ( &$invalid ) {
			if ( $item->UTM_X === '' || $item->UTM_Y === '' ) {
				array_push( $invalid, $item );

				return false;
			}

			return true;
		} );

		// Log invalid items.
		if ( defined( 'WP_DEBUG_LOG' ) && true === WP_DEBUG_LOG && $invalid ) {
			$count = count( $invalid );
			error_log( "Invalid shelter items (${count}): \n" . print_r( $invalid, true ) );
		}

		return $valid;
	}

	// Categories list.
	public function getCategoriesList( WP_REST_Request $request ) {
		$categories = get_terms( [
			'taxonomy'   => RFG_SHELTER_CATEGORY_TAXONOMY,
			'fields'     => 'names',
			// We are hacking multilingual taxonomies, so we must show empty categories.
			'hide_empty' => false,
		] );

		if ( is_wp_error( $categories ) ) {
			return [
				'success' => false,
				'error'   => $categories->get_error_message(),
			];
		}

		return [
			'success' => true,
			'json'    => [
				'categories' => $categories,
			],
		];
	}

	// Districts list.
	public function getDistrictsList( WP_REST_Request $request ) {
		$endpoint = RFG_SHELTERS_API_BASE_URL . 'refugis/districtes';

		return $this->doRequest( $endpoint );
	}

	private function doRequest( $url, $args = [] ) {
		$args = array_merge( [
			'timeout' => 20,
		], $args );

		// Do remote get
		$data     = [
			'success' => true,
		];
		$code     = 0;
		$response = wp_remote_get( $url, $args );

		if ( ! is_wp_error( $response ) && ( $code = wp_remote_retrieve_response_code( $response ) ) == 200 ) {
			$data['json'] = json_decode( wp_remote_retrieve_body( $response ) );
		} else {
			$data['success'] = false;
			$data['error']   = is_wp_error( $response ) ? $response->get_error_message() : $code;
		}

		return $data;
	}

	/**
	 * Map filter keys and group filters:
	 * 'api' - filters to include to shelters API request.
	 * 'post' - filters to apply to WP posts.
	 *
	 * @param $filters
	 *
	 * @return mixed
	 */
	private function mapFilters( $filters ) {
		$filters['api']      = [];
		$filters['meta']     = [];
		$filters['taxonomy'] = [];

		foreach ( self::$filters['api'] as $name => $api_name ) {
			if ( isset( $filters[ $name ] ) ) {
				if ( isset( $filters['api'][ $api_name ] ) ) {
					// If we already have a value we presume that API expects an array of values.
					$filters['api'][ $api_name ]   = [ $filters['api'][ $api_name ] ];
					$filters['api'][ $api_name ][] = $filters[ $name ];
				} else {
					// Plain single value.
					$filters['api'][ $api_name ] = $filters[ $name ];
				}
			}
			unset( $filters[ $name ] );
		}

		foreach ( self::$filters['meta'] as $name => $prop_name ) {
			if ( isset( $filters[ $name ] ) ) {
				$filters['meta'][ $name ] = $filters[ $name ];
			}
			unset( $filters[ $name ] );
		}

		foreach ( self::$filters['taxonomy'] as $name => $prop_name ) {
			if ( isset( $filters[ $name ] ) ) {
				$filters['taxonomy'][ $name ] = $filters[ $name ];
			}
			unset( $filters[ $name ] );
		}

		return $filters;
	}

	// Reserved for sensitive actions that require recent nonce (less than 24 hours old).
	//private function checkNonce( WP_REST_Request $request ) {
	//	$nonce = $request->get_header( 'X-WP-Nonce' );
	//	if ( ! wp_verify_nonce( $nonce, 'wp_rest' ) ) {
	//		die( 'Unauthorized access.' );
	//	}
	//}
}
