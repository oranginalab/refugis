console.log('This is admin JS')

jQuery(document).on("click", "#rfg-scan-shelters", function () {
  // jQuery.ajax(...)
  console.log('Scan shelters...')
  const rfg_scan_button_text = jQuery("#rfg-scan-shelters").text()
  jQuery("#rfg-scan-shelters").attr('disabled', true)
  jQuery("#rfg-scan-shelters").text('Working. Scanning can take a while... ')

  jQuery.ajax({
    url: window.wpRfgApiSettings.root + 'scan-shelters',
    type: 'post',
    headers: {
      'X-WP-Nonce': window.wpRfgApiSettings.nonce,
      'X-Requested-With': 'XMLHttpRequest'
    },
    success: function (response) {
      console.log('Success', response);
      jQuery("#rfg-scan-shelters").text(rfg_scan_button_text)
      jQuery("#rfg-scan-shelters").removeAttr('disabled')
      if (response.status === 'OK') {
        window.alert("Success!\nInserted: " + response.inserted + "\nUpdated: " + response.updated + "\nTrashed: " + response.deprecated)
        window.location.reload()
      } else {
        window.alert("Error:\n" + response.error)
      }
    },
    error: function (error) {
      jQuery("#rfg-scan-shelters").text(rfg_scan_button_text)
      console.log('Error', error.statusText);
      jQuery("#rfg-scan-shelters").removeAttr('disabled')
      window.alert("Error:\n" + error.statusText)
    }
  });

  // https://www.atomicsmash.co.uk/blog/create-like-post-button-in-wordpress/
})
