import axios from "axios";

import Lodash from 'lodash';
window._ = Lodash

try {
  axios.defaults.headers.common = {
    "X-WP-Nonce":
      typeof window.wpRfgApiSettings !== "undefined" ? window.wpRfgApiSettings.nonce : "",
    "X-Requested-With": "XMLHttpRequest"
  };
  window.axios = axios
} catch (e) {
  console.log(e);
}
