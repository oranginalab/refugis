export default {
  // *** Just for development ***
  // If WP is installed in folder we must use it as "base" in vue-router configuration.
  // Should be an empty string when the app is installed on a dedicated domain.
  APP_FOLDER: '',
  // URL of custom tiles. Should be a location outside WP installation folder, where tile files are stored.
  CUSTOM_TILES_URL: 'https://alex3493.asuscomm.com/map-tiles/1935/',
  CUSTOM_BOMB_TILES_URL: 'https://alex3493.asuscomm.com/map-tiles/bombs/'
}
