require('./../bootstrap.js');
import Vue from "vue";
import VueLayers from 'vuelayers'
import 'vuelayers/lib/style.css'
import router from "./router";
import App from './App.vue'

Vue.use(VueLayers);

const app = new Vue({
  el: "#app",
  router,
  render: h => h(App),
  data: {
    state: {
      shelter_selected: null,
      group: null,
      address: null,
      category: null,
      district: null,
      chronology: null
    }
  },
  mounted() {
    // This is the gateway between Vanilla JS (jQuery) and Vue.
    // window.app.$emit('tell-Vue', 'shelter-details-closed')
    // We also support additional parameter (data) for events that require extra info, e.g. read from DOM
    // element "data-some-key" attribute - jQuery.data('someKey')
    this.$on('tell-Vue', (event, data) => {
      // console.log('Root instance state updated', event, data)

      switch (event) {
        case 'shelter-details-closed':
          this.state.shelter_selected = null
          break
        case 'chronology-filter-updated':
          this.state.chronology = data
          break
        // More events here...
        default:
        // Nothing to do.
      }

    })
  },
  methods: {
    // Update root state: merge existing state with new data.
    setState(data) {
      // console.log('Setting root state', data)

      this.state = _.merge({}, this.state, data)
    },
    getState() {
      return _.cloneDeep(this.state)
    }
  },
  provide() {
    // Make root state reactive and accessible for all components via "inject" flow.
    // Make sure that components implement computed properties to resolve root state data,
    // just executing this.root_state_callback() and returning result.
    // Then Vue components can watch computed property changes to reflect them in components.
    return {
      root_state_callback: () => _.cloneDeep(this.state)
    }
  }
});

// Make Vue app instance global.
window.app = app

// Global jQuery handler for shelter links clicks in documentaries.
$(document).on("click", ".rfg-shelter-link", function () {
  var shelter_id = $(this).data("shelter-id");
  if (shelter_id) {
    // We use special query param, so that router can detect that shelter details view
    // is requested from non-map scope.
    window.rfg_router.router.navigate('/?s_link_id=' + shelter_id);
  }
})
