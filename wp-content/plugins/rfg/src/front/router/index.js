import Vue from "vue";
import Router from "vue-router";
import Config from './../../config.js'

import Map from './../components/Map.vue'
import MapClusters from './../components/Map-clusters.vue'

Vue.use(Router);

const router = new Router({
  routes: [
    {
      path: '/',
      name: 'Home',
      component: Map
    },
    {
      path: '/clusters',
      name: 'Clusters',
      component: MapClusters
    },
    // TODO: reserved for dedicated search route.
    // {
    //   path: '/search',
    //   name: "Search",
    //   component: Map
    // },
    {
      // Load Map component in all cases.
      path: '*',
      name: "Default",
      component: Map
    }
  ],
  mode: "history",
  // If we have WPML plugin language code as the first URL segment we append it to base path here.
  // *** IMPORTANT: WPML must be configured to use URL segment for language code.
  // ?lang=xx option will not work!
  base: Config.APP_FOLDER + (window.wpRfgApiSettings.lang ? '/' + window.wpRfgApiSettings.lang : ''),

  // Prevents window from scrolling back to top
  // when navigating between components/views
  scrollBehavior(to, from, savedPosition) {
    if (savedPosition) {
      return savedPosition
    } else {
      return {x: 0, y: 0}
    }
  }
})

export default router;
