const path = require('path');
const VueLoaderPlugin = require('vue-loader/lib/plugin')

// Configuration object.
const config = {
  // Create the entry points.
  // One for frontend and one for the admin area.
  entry: {
    // frontend and admin will replace the [name] portion of the output config below.
    frontend: './src/front/front-index.js',
    admin: './src/admin/admin-index.js'
  },

  // Create the output files.
  // One for each of our entry points.
  output: {
    // [name] allows for the entry object keys to be used as file names.
    filename: 'js/[name].js',
    // Specify the path to the JS files.
    path: path.resolve(__dirname, 'assets')
  },
  resolve: {
    alias: {
      vue$: "vue/dist/vue.esm.js"
    }
  },
  plugins: [
    new VueLoaderPlugin()
  ],
  // Setup a loader to transpile down the latest and great JavaScript so older browsers
  // can understand it.
  module: {
    rules: [
      {
        test: /\.vue$/,
        loader: "vue-loader",
        options: {
          loaders: {
            // Since sass-loader (weirdly) has SCSS as its default parse mode, we map
            // the "scss" and "sass" values for the lang attribute to the right configs here.
            // other preprocessors should work out of the box, no loader config like this necessary.
            scss: "vue-style-loader!css-loader!sass-loader",
            sass: "vue-style-loader!css-loader!sass-loader?indentedSyntax"
          },
          // TODO: not sure about it...
          // transformAssetUrls: {
          //   video: ['src', 'poster'],
          //   source: 'src',
          //   img: 'src',
          //   image: 'xlink:href',
          //   'vl-style-icon': 'src', // this allow vue-loader to transform path in the `src` attribute to `require`
          // },
        }
      },
      {
        // Look for any .js files.
        test: /\.js$/,
        // Exclude the node_modules folder.
        exclude: /node_modules/,
        // Use babel loader to transpile the JS files.
        loader: 'babel-loader'
      },
      {
        test: /\.css$/i,
        use: ['style-loader', 'css-loader'],
      }
    ]
  },
  devtool: 'sourcemap'
}

// Export the config object.
module.exports = config;
