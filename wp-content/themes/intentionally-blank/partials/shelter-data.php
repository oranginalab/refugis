<!--
Shelter information block.
For styles: see wp-content/plugins/rfg/public/css/rfg-public.css
For Vue 'close' event see: wp-content/plugins/rfg/src/front/components/ShelterData.vue
For HTML content see: wp-content/themes/intentionally-blank/partials/shelter-html.php
-->
<script type="text/x-template" id="rfg-shelter-data-template">
    <div>
        <div v-if="show" id="rfg-shelter-data">
            <div v-if="isReady">
                <div v-html="html"></div>
                <button @click="$emit('close')">Close</button>
            </div>
            <div v-if="isError">
                API response error.
            </div>
            <div v-if="isLoading">
                Loading...
            </div>
        </div>
    </div>
</script>
