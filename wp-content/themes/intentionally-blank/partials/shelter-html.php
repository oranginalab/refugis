<?php
/** @var $api_data - raw data received from shelter API */

/** @var $details - nested shelter details data */
?>
<?php if ( isset( $api_data->wp_post ) ): ?>
	<?php
	// Post data shortcuts.
	$post          = $api_data->wp_post;
	$custom_fields = $post->meta;
	?>
    <!--
    WP post core data.
    -->
    <h3><?=$post->post_title?></h3>
    <div>
		<?=$post->post_content?>
    </div>
    <!--
    WP post custom fields data.
    -->
    <ul>
        <li>Description: <?=esc_html( $custom_fields[ RFG_SHELTER_DESCRIPTION_META_KEY ] )?></li>
        <li>Visitable: <?=$custom_fields[ RFG_SHELTER_VISITABLE_META_KEY ] ? 'yes' : 'no'?></li>
        <li>Conservation state: <?=$custom_fields[ RFG_SHELTER_CONSERVATION_STATE_META_KEY ]?></li>
    </ul>
    <hr>
    <!-- End WP post -->
<?php endif; ?>
    <!-- Data from API response. -->
    <h4><?=$api_data->name?></h4>
    <h4><?=$api_data->title?></h4>

    <!-- API response details. -->
<?php rfg_display_nested_data( $details );

// Recursive data display.
function rfg_display_nested_data( $node ) {
	if ( $node->propId ): ?>
        <span><?=$node->value?></span>
		<?php if ( ! empty( $node->children ) ): ?>
			<?php rfg_display_nested_data( $node->children ) ?>
		<?php endif; ?>
	<?php else: ?>
        <ul>
			<?php foreach ( $node as $k => $v ): ?>
                <li>
					<?php if ( ! is_int( $k ) ): ?>
                        <span><?=$k?>: </span>
					<?php endif; ?>
					<?php rfg_display_nested_data( $v ) ?>
                </li>
			<?php endforeach; ?>
        </ul>
	<?php endif;
}
