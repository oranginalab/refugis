<!-- Render shelter information block template script -->
<?php get_template_part( 'partials/rfg/shelter-data' ) ?>

<!-- Render filters block template script -->
<?php get_template_part( 'partials/rfg/filters' ) ?>

<!-- Render search status block template script -->
<?php get_template_part( 'partials/rfg/search-status' ) ?>

<!-- Render footer scripts section -->
<?php wp_footer(); ?>

</body>
</html>
