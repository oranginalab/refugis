<?php
//CSS.
function theme_add_scripts() {
	wp_register_style( 'style', get_template_directory_uri() . '/style.css', [], '20140208', 'all' );
	wp_enqueue_style( 'style' );
}


function custom_rewrite_navigo_routes() {
	add_rewrite_rule('^context-historic/*', 'index.php', 'top');
	add_rewrite_rule('^contexto-historico/*', 'index.php', 'top');
	add_rewrite_rule('^historical-context/*', 'index.php', 'top');
}
add_action('init', 'custom_rewrite_navigo_routes');


/**
 * Get translated object by slug
 * @param $slug
 *
 * @return array|WP_Post|null
 */
function get_translated_page_by_slug( $slug ) {
	//get page by slug
	$page = get_page_by_path($slug);

	// Get the i18n version
	if (!is_object($page)) {
		return false;
	}
	$i18n_page_id = my_translate_object_id( $page->ID, 'page' );
	return get_post($i18n_page_id);
}

//^^^ WPML function to get tranlated version of WPs objects (post, pages, custom post types entries, taxonomies...)
/**
 * Returns the translated object ID(post_type or term) or original if missing
 *
 * @param $object_id integer|string|array The ID/s of the objects to check and return
 * @param $type the object type: post, page, {custom post type name}, nav_menu, nav_menu_item, category, tag etc.
 * @return string or array of object ids
 */
function my_translate_object_id( $object_id, $type ) {
	$current_language= apply_filters( 'wpml_current_language', NULL );
	// if array
	if( is_array( $object_id ) ){
		$translated_object_ids = array();
		foreach ( $object_id as $id ) {
			$translated_object_ids[] = apply_filters( 'wpml_object_id', $id, $type, true, $current_language );
		}
		return $translated_object_ids;
	}
	// if string
	elseif( is_string( $object_id ) ) {
		// check if we have a comma separated ID string
		$is_comma_separated = strpos( $object_id,"," );

		if( $is_comma_separated !== FALSE ) {
			// explode the comma to create an array of IDs
			$object_id     = explode( ',', $object_id );

			$translated_object_ids = array();
			foreach ( $object_id as $id ) {
				$translated_object_ids[] = apply_filters ( 'wpml_object_id', $id, $type, true, $current_language );
			}

			// make sure the output is a comma separated string (the same way it came in!)
			return implode ( ',', $translated_object_ids );
		}
		// if we don't find a comma in the string then this is a single ID
		else {
			return apply_filters( 'wpml_object_id', intval( $object_id ), $type, true, $current_language );
		}
	}
	// if int
	else {
		return apply_filters( 'wpml_object_id', $object_id, $type, true, $current_language );
	}
}
?>
