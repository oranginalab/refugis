<!doctype html>
<html lang="<?php echo $lang = get_bloginfo( "language" ); ?>">
<head>

    <!-- meta name="viewport"
          content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no" -->
	

	<meta content="width=device-width; initial-scale=1.0; maximum-scale=1.0; user-scalable=0;" name="viewport" />

	
    <meta charset="UTF-8">
	
    <title><?php if($lang=="es"){ echo"La ciudad de los refugios"; } elseif($lang=="en-US"){ echo "The city of shelters"; } else { echo "La ciutat dels refugis"; }  ?> | Servei d'Arqueologia de Barcelona</title>

	<?php wp_head(); ?>


    <!-- Slider dates -->
    <script src="https://code.jquery.com/jquery-2.1.1.js"></script>
    <script src="https://code.jquery.com/ui/1.11.1/jquery-ui.js"></script>
    <link rel="stylesheet" href="https://code.jquery.com/ui/1.10.4/themes/flick/jquery-ui.css">
    <script src="<?php echo get_template_directory_uri(); ?>/js/jquery-ui-slider-pips.js"></script>
    <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/jquery-ui-slider-pips.min.css"
          type="text/css" media="all">

    <!-- end slider dates -->

    <!-- slider images -->
    <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/swiper.min.css">
    <script src="<?php echo get_template_directory_uri(); ?>/js/swiper.min.js"></script>
    <!-- end slider images -->
    <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/style.css" type="text/css" media="all">
    <script src="<?php echo get_template_directory_uri(); ?>/js/main.js"></script>
    <script src="<?php echo get_template_directory_uri(); ?>/js/intro.js"></script>

    <!-- Navigo router -->
    <script src="//unpkg.com/navigo@6"></script>
    <script src="<?php echo get_template_directory_uri(); ?>/js/rfg-navigo.js"></script>

    <?php
    // Mobile native Social Sharing
    global $post;

    $options              = get_option( 'rfg_settings', [] );
    $twitter_site_meta    = isset( $options['rfg_twitter_site_meta'] ) ? $options['rfg_twitter_site_meta'] : '@rfg';
    $twitter_creator_meta = isset( $options['rfg_twitter_creator_meta'] ) ? $options['rfg_twitter_creator_meta'] : '@rfg';

    $site_title = 'La ciutat dels refugis';
    $description = 'Catàleg dels refugis antiaeris de Barcelona';
    $default_photo = get_template_directory_uri() . '/images/img-refugis/socials_ca.jpg';
    if (ICL_LANGUAGE_CODE) {
        switch (ICL_LANGUAGE_CODE) {
            case 'es':
                $site_title = 'La ciudad de los refugios';
                $description = 'Catálogo de refugios antiaéreos de Barcelona';
                $default_photo = get_template_directory_uri() . '/images/img-refugis/socials_es.jpg';
                break;

            case 'en':
                $site_title = 'The city of shelters';
                $description = 'Catalogue of Barcelona\'s air raid shelters';
                $default_photo = get_template_directory_uri() . '/images/img-refugis/socials_en.jpg';
                break;

            case 'ca':
            default:
                $site_title = 'La ciutat dels refugis';
                $description = 'Catàleg dels refugis antiaeris de Barcelona';
                $default_photo = get_template_directory_uri() . '/images/img-refugis/socials_ca.jpg';
        }
    }
    ?>
    <!-- Twitter Share -->
    <meta name="twitter:card" content="summary_large_image">
    <meta name="twitter:site" content="<?=$twitter_site_meta?>">
    <meta name="twitter:creator" content="<?=$twitter_creator_meta?>">
    <meta name="twitter:title" content="<?php echo $site_title?>">
    <meta name="twitter:image" content="<?php echo $default_photo;?>">


    <!-- Facebook/LinkedIn Share -->
    <meta property="og:url" content="<?=get_post_permalink( $post->ID )?>"/>
    <meta property="og:type" content="website"/>
    <meta property="og:title" content="<?php echo $site_title?>"/>
    <meta property="og:description" content="<?=$description?>"/>
    <meta property="og:image" content="<?=$default_photo?>"/>
    <meta property="og:image:secure_url" content="<?=$default_photo?>"/>


	<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=G-4HTEMY86XT"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'G-4HTEMY86XT');
</script>



</head>
<body>
<?php if (function_exists('Bcn_brand\bcn_brand_get_html')): ?>
<?php print Bcn_brand\bcn_brand_get_html(); ?>
<?php endif; ?>

