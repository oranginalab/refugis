<?php

get_header();

global $wp;
// Check if we have a query in URL and set intro active class accordingly.
$is_search          = add_query_arg( [ $_GET ], $wp->request );
$intro_active_class = $is_search ? '' : ' active';
?>


<header>

    <div class="corporative">
        <div class="block-one">
            <h1><?php _e( "<span>La ciutat</span><span>dels</span><span>refugis</span>", "refugis" ); ?></h1>
			<?php /*^<p class="sotstitol"><?php _e( "<span>Catàleg dels refugis</span><span>antiaeris de Barcelona</span>", "refugis" ); ?></p> */ ?>
        </div>
        <div class="block-two active">
            <div class="button-global"></div>
            <span class="menu-global"></span>
        </div>
    </div>
    <nav class="global">
        <ul>
            <li class="inici-value"><?php _e( 'Inici', 'refugis' ); ?></li>
            <li class="destacat cerca-value active"><?php echo _e( '<span>Explora</span> el Mapa', 'refugis' ); ?></li>
            <li class="destacat context-value"><?php echo _e( 'Context històric', 'refugis' ); ?></li>
            <li class="about-value"><?php echo _e( 'Sobre el projecte', 'refugis' ); ?></li>
            <li class="credits-value"><?php echo _e( 'Crèdits', 'refugis' ); ?></li>
            <li class="language"><?php do_action('icl_language_selector'); ?></li>
			<li class="inidcacions">
					<div>
		
       		<p class="logo-bbl"><img src="/arqueologiabarcelona/refugis/wp-content/themes/refugis/images/bbl_300.png"  alt="Barcelona Bombardejada"/></p>
        </div>
				</li>
        </ul>
			
	
    </nav>

    <!-- p class="info-butto"><?php _e( "Informació", "refugis" ); ?></p -->

</header>
<main>
    <!-- SECTION INTRO -->
    <section class="intro  <?php echo $intro_active_class; ?>">
		<?php get_template_part( 'partials/intro' ); ?>

    </section>
    <!-- SECTION MAPA -->
    <!--section class="mapa">
	</section-->

    <!-- SECTION CERCA -->
    <section class="cerca">
        <h2 class="section-title sobre_mapa"><?php _e('<span>Explora</span> el Mapa','refugis'); ?></h2>
         <h3 class="proposal"><?php _e('<span>Propostes</span> de cerca','refugis'); ?></h3>
		<?php get_template_part( 'partials/mapa' ); ?>



        <?php /* ------------- PROPOSTES ---------------*/ ?>


        <div class="llista-propostes">
            <ul>
				<?php
				$custom_agrupacions = get_terms( 'agrupacio', [
					// We are hacking multilingual taxonomies, so we must show empty groups.
					'hide_empty' => false,
				] ); /* llistem les agrupacions amb descripció */
				$active             = 1;
				$valor              = "";
				foreach ( $custom_agrupacions as $custom_agrupacio ) { ?>
                    <li id="<?php echo $custom_agrupacio->term_id; ?>">
                        <p class="nom"><?php echo $custom_agrupacio->name; ?></p>


                    </li>
				<?php } ?>
            </ul>
        </div>

		<?php
		$custom_agrup_descs = get_terms( 'agrupacio', [
			// We are hacking multilingual taxonomies, so we must show empty groups.
			'hide_empty' => false,
		] );
		$active             = 1;
		$valor              = "";
		foreach ( $custom_agrup_descs as $custom_agrup_desc ) { ?>
            <div class="llista-propostes-descripcio" id="agrup-desc-<?php echo $custom_agrup_desc->term_id; ?>">
                <p class="nom"><?php echo $custom_agrup_desc->name; ?></p>
                <div class="descripcio">
					<?php echo $custom_agrup_desc->description; ?>


                </div>
                <div class="ampliar active" id="ampliar-<?php echo $custom_agrup_desc->term_id; ?>">Seguir llegint</div>
                <p class="button"><input type="button"
                                         data-group-name="<?php echo $custom_agrup_desc->name; ?>"
                                         class="input-propostes"
                                         value="<?php _e( "Veure refugis", "refugis" ) ?>"></p>

                <div class="contingut-proposta" id="agrup-cont-agrup-desc-<?php echo $custom_agrup_desc->term_id; ?>">
					<?php $origen = 'agrupacio_' . $custom_agrup_desc->term_id;
					$long_desc    = get_field( "descripcio_amplia", $origen );
					echo $long_desc; ?>
                    <div class="close"></div>
                </div>

                <div class="close"></div>

            </div>
		<?php } ?>


		<?php /* ------------- END PROPOSTES ---------------*/ ?>


        <div class="llegenda">

            <ul>
                <li class="bomba"><span></span><?php _e( 'Impacte bombardeig', 'refugis' ); ?></li>
                <li class="refugi"><span></span><?php _e( 'Refugi', 'refugis' ); ?></li>
            </ul>
        </div>


        <p class="credits-mapa"><?php _e( 'Plànol de Barcelona, 1935', 'refugis' ); ?>.
            <span><?php _e( 'Ajuntament de Barcelona', 'refugis' ); ?>. <?php _e( 'Oficina del Pla de la Ciutat', 'refugis' ); ?></span>
        </p>
		<?php // get_template_part( 'partials/cerca' ); ?>
    </section>

    <!-- SECTION CONTEXT -->
    <section class="context">
		<?php get_template_part( 'partials/context' ); ?>
    </section>

    <!-- SECTION ABOUT -->
    <section class="about">
		<?php get_template_part( 'partials/about' ); ?>
    </section>

    <!-- SECTION Credits -->
    <section class="credits">
		<?php get_template_part( 'partials/credits' ); ?>
    </section>

	<!-- SECTION TEXT-INTRO -->
	<section class="text_intro">
	<?php get_template_part( 'partials/text_intro' ); ?>
	</section>
	
	
    <!-- Global loading spinner -->
    <div class="loading"><div class="loading-inset"><img src="<?php echo get_template_directory_uri(); ?>/images/spinner.png" alt=""/></div></div>
</main>
<div class="help">?</div>
<div class="help-content">
    <div class="close"></div>
    <ol>
        <li><?php _e('Menú general', 'refugis'); ?>
            </li>
        <li><?php _e('Títol de secció', 'refugis'); ?>
            </li>
        <li><?php _e('Cercador de Districte, any i nom', 'refugis'); ?>
            </li>
        <li><?php _e('Agrupacions de refugis per temàtica.', 'refugis'); ?>
            </li>
        </ol>
    </div>
<footer></footer>
<?php
get_footer();
?>
