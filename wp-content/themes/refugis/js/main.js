// JavaScript Document
$(document).ready(function () {

// CONTROL ALÇADA IPHONE // DEPURAR TOMAS PLIS ( REPETICIÓ INNECESSÀRIA)

  let vh = window.innerHeight * 0.01;
  document.documentElement.style.setProperty('--vh', `${vh}px`);
  document.documentElement.style.setProperty('--vh-noresize', `${vh}px`);

  window.addEventListener('resize', () => {
    // We execute the same script as before
    let vh = window.innerHeight * 0.01;
    document.documentElement.style.setProperty('--vh', `${vh}px`);
  });


// CONTROL ALÇADA FOOTER i LLEGENDA

  var h_cerca = $('section.cerca #rfg-filters > header').outerHeight();  // calculem alçada footer
  document.documentElement.style.setProperty('--h_cerca', `${h_cerca}px`);

  var h_llegenda = $('.llegenda').outerHeight();  // calculem alçada footer
  document.documentElement.style.setProperty('--h_llegenda', `${h_llegenda}px`);

  /* CONTROL ALÇADA CAPÇALERA FITXA REFUGI

var h_header_refugi = $('article.detail-fitxa header').outerHeight();  // calculem alçada capçalera (header ) fitxa refugi
document.documentElement.style.setProperty('--h_header_refugi', `${h_header_refugi}px`);
*/

// INICIALITZACIÓ

  window.rfg_router.init();

  $(document).on("click", "p.jump, .intro .title-web", function (e) {
    e.preventDefault();
    $(".intro,.global li").removeClass("active");
    $(".cerca-value").addClass("active");
    $("#brand").addClass("inactive");
    $("h2.section-title.sobre_mapa").removeClass("hidden");
    $(".contingut-proposta,.llista-propostes-descripcio, .llista-propostes, .proposal").removeClass("active");
	$(".text_intro").addClass("active");

  });


    // HELP 
      $(".help").click(function () {
           $("body").addClass("global-help");
      });
    
    
      $(".help-content .close").click(function () {
           $("body").removeClass("global-help");
      });
// MENU GLOBAL
	
	
	
	
// MEMORIA 

	$(".logo-bbl").click(function () {
           $(".text_intro").addClass("active");
           $(".text_intro").removeClass("open");
      });
	
	
	$(".text_intro .close").click(function () {
           $(".text_intro").removeClass("active");
           $(".text_intro").addClass("open");
		
      });
	
	
	
	
  //activació menu

  $(".button-global,.menu-global").click(function () {
    $(".button-global,.menu-global, .global ul").toggleClass("active");
    $("section.cerca").removeClass("active");
    $("h2.section-title,.block-one h1").removeClass("cerca-active");
  });


  //Canvi destacat menu

  $(".global li").click(function () {
    $(".button-global,.menu-global, .global ul,.global li").removeClass("active");
    $(this).addClass("active");
    $('html, body').animate({scrollTop: '0px'}, 0);
  });


  // intro

  $(".global li.inici-value").click(function () {
    $(".intro").addClass("active");
    $("section.context,section.about,section.credits").removeClass("active");
    $("#brand").removeClass("inactive");

    // Reset router.
    window.rfg_router.router.navigate('/');
  });

  // Context

  $(".global li.context-value").click(function () {
    window.rfg_router.router.navigate('/context-historic');
    $("h2.section-title.sobre_mapa").addClass("hidden");

  });

  // mapa

  $(".global li.cerca-value").click(function () {
    window.rfg_router.router.navigate('/');
    $("h2.section-title.sobre_mapa").removeClass("hidden");
    $(".contingut-proposta,.llista-propostes-descripcio, .llista-propostes, .proposal").removeClass("active");
      $(".capitol-button,.context .menu .swiper-button").removeClass("inactive");
      
  });

  $("h3.proposal").click(function () {
    $(".llista-propostes").toggleClass("active");
    $(this).toggleClass('active');
    $(".contingut-proposta").removeClass('active'); /* segon nivell -text llarg */
  });


  $(document).on("click", "h3.proposal.onfire", function () {
    $(this).removeClass('onfire');
    $(".llista-propostes-descripcio, .llista-propostes li").removeClass('active');
  });

  $(document).on("click", ".input-propostes", function () {
    var group_name = $(this).data('group-name');
    console.log('Search proposals route', group_name);

    // Reset search proposals controls.
    $(".llista-propostes-descripcio,.contingut-proposta, .llista-propostes, .llista-propostes li, .proposal").removeClass('active');

    // Force navigation even if URL was not changed.
    window.rfg_router.router._lastRouteResolved.query = null
    window.rfg_router.router.navigate('/?group=' + encodeURIComponent($(this).data('group-name')));
  });


  $(".llista-propostes li .nom").click(function () {
      
    $(".contingut-proposta").removeClass('active'); /* segon nivell -text llarg */
    var $prop = "#agrup-desc-" + $(this).parent().attr('id');
    if ($($prop).hasClass('active')) {
      $($prop).removeClass('active');
    } else {
      $(".llista-propostes-descripcio").removeClass('active');
      $($prop).addClass('active');
      $(".ampliar").addClass('active');
    }
    $("h3.proposal.active").addClass('onfire');


    if ($(this).parent().hasClass('active')) {
      $(this).parent().removeClass('active');
    } else {
      $(".llista-propostes li").removeClass('active');
      $(this).parent().addClass('active');
    }
  });
     $(".contingut-proposta .close, .llista-propostes-descripcio .close").click(function () {
          $(".contingut-proposta, .llista-propostes-descripcio, .llista-propostes li").removeClass('active');
  });
  $(".ampliar").click(function () {
    $(".ampliar").removeClass('active');
    var $prop2 = "#agrup-cont-" + $(this).parent().attr('id');
    
    if ($($prop2).hasClass('active')) {
      $($prop2).removeClass('active');
    } else {
      $(".contingut-proposta").removeClass('active');
      $($prop2).addClass('active');
    }
   


  });
  // contexte ca

  $(".barcelona-sota-les-bombes h3").click(function () {
    // Do not allow documents block switch if we have an open article.
   // if (!$(".close-documental").hasClass("active")) {
      $(".barcelona-sota-les-bombes").addClass("active");
      $(".la-vida-als-refugis").removeClass("active");
      $(".lendema").removeClass("active");
   // }
  });
  $(".la-vida-als-refugis h3").click(function () {
    // Do not allow documents block switch if we have an open article.
   // if (!$(".close-documental").hasClass("active")) {
      $(".barcelona-sota-les-bombes").removeClass("active");
      $(".la-vida-als-refugis").addClass("active");
      $(".lendema").removeClass("active");
   // }
  });
  $(".lendema h3").click(function () {
    // Do not allow documents block switch if we have an open article.
   // if (!$(".close-documental").hasClass("active")) {
      $(".barcelona-sota-les-bombes").removeClass("active");
      $(".la-vida-als-refugis").removeClass("active");
      $(".lendema").addClass("active");
   // }
  });
    
    
  // contexte es

  $(".barcelona-bajo-las-bombas h3").click(function () {
    // Do not allow documents block switch if we have an open article.
   // if (!$(".close-documental").hasClass("active")) {
      $(".barcelona-bajo-las-bombas").addClass("active");
      $(".la-vida-en-los-refugios").removeClass("active");
      $(".el-dia-siguiente").removeClass("active");
   // }
  });
  $(".la-vida-en-los-refugios h3").click(function () {
    // Do not allow documents block switch if we have an open article.
   // if (!$(".close-documental").hasClass("active")) {
      $(".barcelona-bajo-las-bombas").removeClass("active");
      $(".la-vida-en-los-refugios").addClass("active");
      $(".el-dia-siguiente").removeClass("active");
   // }
  });
  $(".el-dia-siguiente h3").click(function () {
    // Do not allow documents block switch if we have an open article.
   // if (!$(".close-documental").hasClass("active")) {
      $(".barcelona-bajo-las-bombas").removeClass("active");
      $(".la-vida-en-los-refugios").removeClass("active");
      $(".el-dia-siguiente").addClass("active");
   // }
  });
    
    
  // contexte en

  $(".barcelona-under-bombardment h3").click(function () {
    // Do not allow documents block switch if we have an open article.
   // if (!$(".close-documental").hasClass("active")) {
      $(".barcelona-under-bombardment").addClass("active");
      $(".life-shelters").removeClass("active");
      $(".after-war").removeClass("active");
   // }
  });
  $(".life-shelters h3").click(function () {
    // Do not allow documents block switch if we have an open article.
   // if (!$(".close-documental").hasClass("active")) {
      $(".barcelona-under-bombardment").removeClass("active");
      $(".life-shelters").addClass("active");
      $(".after-war").removeClass("active");
   // }
  });
  $(".after-war h3").click(function () {
    // Do not allow documents block switch if we have an open article.
   // if (!$(".close-documental").hasClass("active")) {
      $(".barcelona-under-bombardment").removeClass("active");
      $(".life-shelters").removeClass("active");
      $(".after-war").addClass("active");
   // }
  });

    

  $(".capitol-button").click(function () {
    $(".capitol-button,.context .menu .swiper-button").addClass("inactive");
  });

  // about

  $(".global li.about-value").click(function () {
    window.rfg_router.router.navigate('/sobre-el-projecte');
    $("h2.section-title.sobre_mapa").addClass("hidden");

  });

  // credits

  $(".global li.credits-value").click(function () {
    window.rfg_router.router.navigate('/credits');
    $("h2.section-title.sobre_mapa").addClass("hidden");
  });


// ACCIONS CERCA

  $(".button-cerca, .buttons").click(function () {
    $("section.cerca").toggleClass("active");
    $("h2.section-title,.block-one h1").toggleClass("cerca-active");

  });
  $(".llegenda-title").click(function () {
    $(".llegenda").toggleClass("active");

  });

  // DELEGATE EVENT TO FILTERS CONTAINER (ALEX)

  // We render typologies list in a Vue component after document ready event is triggered.
  // We have to listen to this event in container.
  $('#rfg-filters').on("click", ".tipologia-active, .tipologies ul li a", function () {
    var $el = $(this)
    // Close other dropdowns.
    $(".tipologies").filter(function () {
      return $(this).find($el).length === 0
    }).removeClass("active");
    // Toggle current dropdown active state.
    $el.parents(".tipologies").toggleClass("active");
  });

// ACCIONS SLIDER (FILTRES)
  $(".slider").slider({
    min: 1936,
    max: 1946,
    range: true,
    values: [1936, 1946],
    // EMIT UPDATE EVENT TO VUE (ALEX)
    slide: function (event, ui) {
      window.app.$emit('tell-Vue', 'chronology-filter-updated', ui.values)
    }
  })
  // Tell Vue - initial values.
  window.app.$emit('tell-Vue', 'chronology-filter-updated', $(".slider").slider("values"));
  // EMIT UPDATE EVENT TO VUE (ALEX)

  $(".slider").slider("pips", {
    rest: "label"
  })

  $(".slider").slider("float");


// ACCIONS CONTEXT

  $(".button-context").click(function () {
    $("section.context,.backgroud-context, .button-context").toggleClass("active");
    $("section.cerca,.button-cerca,nav").removeClass("active");
    $("nav").addClass("active");
    $("nav").removeClass("on-stage");

    $(".backgroud-context").one("webkitTransitionEnd otransitionend oTransitionEnd msTransitionEnd transitionend", // Esperem que double-click acabi transicions
      function (event) {
        $("nav").addClass("on-stage");
      });
  });


// ACTIVACIO I DESACTIVACIO -> "CLOSE" BOTÓ DE DETALL i DOCUMENTAL

  $(document).on("click", ".detail.active .close", function (e) {

    $(".detail, .documental").empty();
  });


  // desactivar i buidar divs .documentsl i .fitxa


  $(document).on("click", ".close-documental, .context .menu .active  h3, .context .menu .swiper-button-next", function (e) {
    $(".section-title").removeClass("documental-active");
    $(".capitol-button,.context .menu .swiper-button").removeClass("inactive");
    window.rfg_router.router.navigate('/context-historic/');

    // *** Moved to Navigo router.
    // $('.documental').addClass("clean");
    // $('.close-documental').removeClass("active");
    // $(".documental.clean").one("webkitTransitionEnd otransitionend oTransitionEnd msTransitionEnd transitionend", // Esperem que double-click acabi transicions
    //   function (event) {
    //     $(".block-two").addClass("active");
    //     $(".documental.clean").empty();
    //     $(".documental.clean").removeClass("clean active");
    //   });

  });


  $(document).on("click", ".fitxa .close", function (e) {
    $('.fitxa').addClass("clean");
    $(".fitxa.clean").one("webkitTransitionEnd otransitionend oTransitionEnd msTransitionEnd transitionend", // Esperem que double-click acabi transicions
      function (event) {
        $(".block-two").addClass("active");
        $(".fitxa.clean").empty();
        $(".fitxa.clean").removeClass("clean active");
      });

  });


});


$(window).scroll(function () {
  var width = $(window).width();
  if (width < 1024) {

    if ($(document).scrollTop() > 2/* window.innerHeight * 0.2 */) {

      $('.block-one,.block-two, .close-documental, h2.section-title').addClass('soft');
      /*    clearTimeout($.data(this, 'scrollTimer'));
         $.data(this, 'scrollTimer', setTimeout(function() {
            $('.block-one,.block-two, .close-documental, h2.section-title').removeClass('soft');
         }, 500));
           */
    } else {
      $('.block-one,.block-two, .close-documental, h2.section-title').removeClass('soft')
    }
  }
});


function logo_moveOn() {  /* Tomasssss  plissss... context.php lin 2 */

  if ($("header").scrollTop() > 0) {
    $(".block-one,.block-two").addClass('soft');
  } else {
    $(".block-one,.block-two").removeClass('soft');
  }
}

// CONTROL LOADING STATE FROM MULTIPLE ACTIONS (ALEX).
function setLoadingStatus(value, isError) {
  // console.log('Global loading status handler', value, isError);

  var loadingCount = window.rfg_loading_count || 0;
  if (isError) {
    // If error we force reset loading count, so that we never stay stalled with frozen spinner.
    loadingCount = 0;
  } else {
    // Update loading count.
    loadingCount = value ? ++loadingCount : --loadingCount;
  }
  // Update active class.
  if (loadingCount > 0) {
    $(".loading").addClass('active');
  } else {
    $(".loading").removeClass('active');
  }
  // Update global loading count.
  window.rfg_loading_count = loadingCount < 0 ? 0 : loadingCount;

  // console.log('Current global loading count', window.rfg_loading_count)
}

// CONTROL LOADING STATE FROM MULTIPLE ACTIONS (ALEX).

//----------------------------------

// TOMSEN clos


// CARREGA DOCUMENTALS PROVISIONAL  (TOMAS)

$(document).on("click", ".menu a, .next-doc a", function (e) {
  // $(".section-title").addClass("documental-active");  /* titol de la secció ( explora , context) */
  e.preventDefault();
  var href = $(this).attr('href');
  console.log('Document href', href);
  var slug = $.grep(href.split('/'), function (s) {
    return !!s;
  }).pop();

  //^^^ Remove language prefix from slug
  var slug_arr = slug.split('-');
  // slug_arr.shift();

  //*** Add language prefix to slug (proposal)
  slug_arr.unshift(window.wpRfgApiSettings.lang);
  slug = slug_arr.join('-');

  // console.log('Document slug', slug);
  window.rfg_router.router.navigate('/context-historic/' + slug);

  // *** Moved to Navigo router.
  // $('.documental').load(href, function (responseTxt, statusTxt, xhr) {
  //
  //   if (statusTxt == "success")
  //     $('html, body').animate({scrollTop: '0px'}, 0);
  //   $('.documental').addClass("active");
  //   $(".documental").one("webkitTransitionEnd otransitionend oTransitionEnd msTransitionEnd transitionend", // Esperem que double-click acabi transicions
  //     function (event) {
  //       $(".block-two").removeClass("active");
  //       $(".close-documental").addClass("active");
  //       $(".loading").removeClass('active');
  //     });
  //
  //   if (statusTxt == "error") alert("Error: " + xhr.status + ": " + xhr.statusText);
  // });
});

// CARREGA REFUGIS PROVISIONAL  (TOMAS)

// EMIT EVENTS TO VUE (ALEX)

$(document).on("click", "#shelter-details-close-btn", function (e) {
  $(".section-title").removeClass("documental-active");
  setTimeout(function () {
    // Give some time for CSS transitions to run.
    window.app.$emit('tell-Vue', 'shelter-details-closed');
  }, 1000);
});

// EMIT EVENTS TO VUE (ALEX)


$(document).on("click", ".marker a", function (e) {
  setLoadingStatus(true);

  e.preventDefault();
  var href2 = $(this).attr('href');

  $('.fitxa').load(href2, function (responseTxt, statusTxt, xhr) {

    if (statusTxt == "success") {
      $('html, body').animate({scrollTop: '0px'}, 0);
    }
    $('.fitxa').addClass("active");
    $(".fitxa").one("webkitTransitionEnd otransitionend oTransitionEnd msTransitionEnd transitionend", // Esperem que double-click acabi transicions
      function (event) {
        setLoadingStatus(false);
      });

    if (statusTxt == "error") alert("Error: " + xhr.status + ": " + xhr.statusText);
  });
});
