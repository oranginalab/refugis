(function ($) {
  'use strict';
  window.rfg_router = {
    options: {
      root: null,
      useHash: false,
      hash: '#!'
    },
    router: null,
    /**
     * Routes for Navigo.
     *
     * All routes listed below will be registered on router init.
     * We add Map (/) route explicitly at the end.
     *
     * We rely on "name" property in order to substitute routs on the fly with i18n versions.
     * IMPORTANT: we must always prepend route names with language code part, e.g. en-..., es-..., ca-...
     */
    routes: {
      en: [
        {
          route: 'about',
          name: 'en-about',
          handler: 'about'
        },
        {
          route: 'project-credits',
          name: 'en-credits',
          handler: 'credits'
        }, {
          route: 'historical-context',
          name: 'en-context',
          handler: 'context'
        },
        {
          route: 'historical-context/:slug',
          name: 'en-document',
          handler: 'context'
        }
      ],
      es: [
        {
          route: 'sobre-el-proyecto',
          name: 'es-about',
          handler: 'about'
        },
        {
          route: 'creditos',
          name: 'es-credits',
          handler: 'credits'
        },
        {
          route: 'contexto-historico',
          name: 'es-context',
          handler: 'context'
        },
        {
          route: 'contexto-historico/:slug',
          name: 'es-document',
          handler: 'context'
        }
      ],
      ca: [
        {
          route: 'sobre-el-projecte',
          name: 'ca-about',
          handler: 'about'
        },
        {
          route: 'credits',
          name: 'ca-credits',
          handler: 'credits'
        },
        {
          route: 'context-historic',
          name: 'ca-context',
          handler: 'context'
        },
        {
          route: 'context-historic/:slug',
          name: 'ca-document',
          handler: 'context'
        }
      ]
    },

    init: function (options) {
      $.extend(this.options, options || {});

      this.router = new Navigo(this.options.root, this.options.useHash, this.options.hash);
      this.router.hooks({
        before: function (done, params) {
          // Translate routes (i18n).
          var current = this.router.lastRouteResolved();
          // console.log('Current route', current);

          // Find route version in correct language.
          var matches = $.grep(rfg_router.routes[window.wpRfgApiSettings.lang], function (item) {
            return item.name === window.wpRfgApiSettings.lang + '-' + current.name.split('-').slice(1).join('-');
          });

          if (matches.length) {
            // We have the match.
            var route = matches[0].route;
            // Populate parameters (if any).
            route = rfg_router.router.generate(matches[0].name, params);
            console.log('i18n route:', route)

            // Do navigation (we pause router to replace route in history).
            rfg_router.router.pause();
            rfg_router.router.navigate(route);
            rfg_router.router.resume();
          }

          // Reset menu items active.
          $(".global li").removeClass("active");
          done();
        }.bind(this)
      });

      this.initRoutes();
    },
    initRoutes: function () {
      // i18n routes.
      $.each(rfg_router.routes, function (lang, routes) {
        $.each(routes, function (index, item) {
          this.router.on(item.route, {
            as: item.name,
            uses: function (params, query) {
              rfg_router[item.handler](params);
            }
          })
        }.bind(this))
      }.bind(this))

      this.router
        // Map.
        .on('', {
          as: 'map',
          uses: function (params, query) {
            console.log('Default view - map. Query: ', query);

            // Programmatic navigation to shelter details from other scopes.
            var shelter_id_match = query.match(/s_link_id=([0-9]+)/);
            if (shelter_id_match && shelter_id_match[1]) {
              // We have shelter ID in query. Get current filters.
              var currentFilters = window.app.getState();
              $.each(currentFilters, function (k, v) {
                // Only choose non-empty.
                if (v === null || k === 'shelter_selected') {
                  delete (currentFilters[k]);
                }
              })
              // console.log('Sanitized filters', currentFilters);

              // We have to replace route in Vue-router, so that special s_link_id query
              // never goes to browser history.
              window.app.$router.replace({
                path: '/',
                // Preserve current filters.
                query: $.extend({}, currentFilters, {
                  s_id: shelter_id_match[1]
                })
              }).catch(err => {
                // Actually not an error.
              });
            }

            // Programmatic navigation to search proposals (group search) from other scopes.
            var group_search_match = query.match(/group=([^&]*)/);
            if (group_search_match && group_search_match[1]) {
              console.log('Group search route', group_search_match[1]);
              window.app.$router.replace({
                path: '/',
                query: {
                  group: decodeURIComponent(group_search_match[1]),
                  // Force navigation even if group slug didn't change.
                  r: Date.now()
                }
              }).catch(err => {
                // Actually not an error.
              });
            }

            $("section.context,section.about,section.credits").removeClass("active");

            // Enable search status.
            $("#rfg-search-status").removeClass("hidden");

            // Set active menu item.
            $("li.cerca-value").addClass("active");

            // Map is default route, so we cannot hide #brand right away because intro slider might
            // be still visible. Skip intro action hides brand header, but we also should hide it
            // when intro timeout is over. With the interval below we can handle it correctly at a
            // reasonable cost.
            var hide_header_interval = setInterval(function () {
              if (!$(".intro").hasClass("active")) {
                // Hide brand header.
                $("#brand").addClass("inactive");
                clearInterval(hide_header_interval)
              }
            }, 1000)
          }
        })
        // Default route.
        // .on(function () {
        //   console.log('Route not found or out of scope.');
        //
        // })
        // TODO: add notFound route(?)
        .resolve();
    },
    about: function () {
      console.log('About route.');
      $("section.about").addClass("active");
      $("section.intro,section.context,section.credits").removeClass("active");

      // Hide search status.
      $("#rfg-search-status").addClass("hidden");

      // Set active menu item.
      $("li.about-value").addClass("active");

      // Hide brand header.
      $("#brand").addClass("inactive");
    },
    credits: function () {
      console.log('Credits route.');
      $("section.credits").addClass("active");
      $("section.intro,section.context,section.about").removeClass("active");

      // Hide search status.
      $("#rfg-search-status").addClass("hidden");

      // Set active menu item.
      $("li.credits-value").addClass("active");

      // Hide brand header.
      $("#brand").addClass("inactive");
    },
    context: function (params) {
      if (params && params.slug) {
        //^^^ Add language prefix to documentary slug
        // var slug = window.wpRfgApiSettings.lang + '-' + params.slug;

        //*** Remove language prefix from documentary slug (proposal)
        var slug = params.slug.split('-').slice(1).join('-');

        var href = window.wpUrl.base + 'documental/' + slug;
        console.log('Document route', href);

        setLoadingStatus(true);

        $('.documental').load(href, function (responseTxt, statusTxt, xhr) {
          if (statusTxt == "success") {
            $('html, body').animate({scrollTop: '0px'}, 0);
          }

          // Event handler defined in context route breaks global menu classes.
          $(".documental.clean").off("webkitTransitionEnd otransitionend oTransitionEnd msTransitionEnd transitionend");

          $("section.context").addClass("active");

          $('.documental').addClass("active").removeClass("clean");

          $(".block-two").removeClass("active");
          $(".close-documental").addClass("active");

          // Initialize swipers just in case we get here directly from URL.
          initSwipers(href);

          // In document routes we disable swipe buttons. See $(".capitol-button").click() in main.js.
          $(".capitol-button,.context .menu .swiper-button").addClass("inactive");

          setLoadingStatus(false);

          if (statusTxt == "error") alert("Error: " + xhr.status + ": " + xhr.statusText);
        });

        // Set active menu item.
        $("li.context-value").addClass("active");
      } else {
        console.log('Context route.');
        $("section.context").addClass("active");
        $("section.intro,section.about,section.credits").removeClass("active");

        $('.documental').addClass("clean");
        $('.close-documental').removeClass("active");

        // console.log('Context route: set transition end handler');

        $(".documental.clean").one("webkitTransitionEnd otransitionend oTransitionEnd msTransitionEnd transitionend", // Esperem que double-click acabi transicions
          function (event) {

            // console.log('Context route: transition ended');

            $(".block-two").addClass("active");
            $(".documental.clean").empty();
            $(".documental.clean").removeClass("clean active");
          });

        // Set active menu item.
        $("li.context-value").addClass("active");

        initSwipers();
      }

      // Hide search status.
      $("#rfg-search-status").addClass("hidden");

      // Hide brand header.
      $("#brand").addClass("inactive");
    }
  }

  function initSwipers(documentHref) {
    $(".data-swiper").each(function () {

      if (this.swiper) {
        // Already initialized, continue loop.
        return;
      }

      // Get unique slug for correct init data.
      var swiper_slug = $(this).data("swiper-slug");

      if (documentHref) {
        // We have to set initial slide that reflects current document content.
        var slides = $(this).find(".swiper-slide");

        // Find correct slide.
        var initialSlide = null;
        var found = false;
        slides.each(function (index, el) {
          // Just is case we ignore trailing slashes.
          if ($(el).data('href').replace(/\/$/, "") === documentHref.replace(/\/$/, "")) {
            initialSlide = index;
            found = true;
          }
        })
      } else {
        // General document route, set first slide active.
        initialSlide = 0
      }

      var swiper = new Swiper(".swiper-container-" + swiper_slug, {
        breakpoints: {
          1024: {
            direction: "vertical"
          }
        },
        pagination: {
          el: ".swiper-pagination-" + swiper_slug,

        },
        navigation: {
          nextEl: ".swiper-button-next-" + swiper_slug,
          prevEl: ".swiper-button-prev-" + swiper_slug,
        },
        initialSlide: initialSlide
      });

      // If we have found active document make sure we also update active block.
      if (documentHref && found) {
        var activeSection = $(this).data('swiper-slug')

        $(".documentals-block").removeClass("active");
        $("." + activeSection).addClass("active");
      }
    })
  }
})(jQuery);
