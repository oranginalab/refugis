/**
 * Define shelter points styling here.
 *
 * We can dynamically apply styles according to shelter data, both API response and WP post meta.
 * Styles defined here can then be used in shelterPoints computed property.
 *
 * See available styling options in OL styles docs.
 *
 * See http://35.187.1.125:8082/api/refugis/tipologies for available typologies.
 */
window.rfgShelterStyles = {
  default: {
    imageRadius: 8,
    strokeWidth: 2,
    // Use both strokeColor and fillColor to set different colors for outline and fill.
    strokeColor: 'rgba(0,0,0,1)',
    fillColor: 'rgba(0,0,0,0.6)'
  },
  'Galeria de mina': {
    imageRadius: 9,
    strokeWidth: 2,
    // Use both strokeColor and fillColor to set different colors for outline and fill.
    strokeColor: 'rgba(0,0,0,1)',
    fillColor: 'rgba(0,0,0,0.6)'
  },
  'Altres': {
    imageRadius: 9,
    strokeWidth: 2,
    // Use both strokeColor and fillColor to set different colors for outline and fill.
    strokeColor: 'rgba(0,0,0,1)',
    fillColor: 'rgba(0,0,0,0.6)'
  },
  'Estructura construïda reutilitzada': {
    imageRadius: 9,
    strokeWidth: 2,
    // Use both strokeColor and fillColor to set different colors for outline and fill.
    strokeColor: 'rgba(0,0,0,1)',
    fillColor: 'rgba(0,0,0,0.6)'
  },
  'Independent / Cel·lular': {
    imageRadius: 9,
    strokeWidth: 2,
    // Use both strokeColor and fillColor to set different colors for outline and fill.
    strokeColor: 'rgba(0,0,0,1)',
    fillColor: 'rgba(0,0,0,0.6)'
  },
  // Last shelter selected (highlighted)
  highlighted: {
    imageRadius: 14,
    strokeWidth: 2,
    strokeColor: 'rgba(221,27,27,1)',
    fillColor: 'rgba(221,27,27,0.6)',
    zIndex: 100
  },
  // Add more styling options here.
  cluster_style: {
    imageRadius: 9,
    strokeColor: 'rgba(0,0,0,1)',
    fillColor: 'rgba(0,0,0,0.6)',
    textFillColor: '#fff'
  }
}
