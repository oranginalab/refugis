<h2 class="section-title">
    <span><?php _e( "<span>Context</span> històric", "refugis" ); ?></span>
</h2>

<header onscroll="logo_moveOn();">


    <nav>

        <div class="menu">

			<?php
			
			$veure_cap =  __( "Veure el capitol", "refugis" );

			$custom_terms = get_terms( 'categoria' );
			$active       = 1;
			$valor        = "";
			foreach ( $custom_terms as $custom_term ) {
				wp_reset_query();
				$args = [
					'post_type' => 'documental',
					'order'     => 'ASC',
					'tax_query' => [
						[
							'taxonomy' => 'categoria',
							'field'    => 'slug',
							'terms'    => $custom_term->slug,
						],
					],
				];

				$loop = new WP_Query( $args );
				if ( $loop->have_posts() ) {
					if ( $active == 1 ) {
						$valor = "active";
					} else {
						$valor = "";
					}

					echo '<div class="' . $valor . ' documentals-block ' . $custom_term->slug . '"><h3>' . $custom_term->name . '</h3>';
					$active = 0;
					/*
					 * Do not remove, nor change class="data-swiper" and data-swiper-slug attribute from the element below.
					 * We rely on these properties when we init Swipers in router.
					 * See wp-content/themes/refugis-2020-v6/js/rfg-navigo.js:280
					 */
					echo ' <div data-swiper-slug="' . $custom_term->slug . '" class="data-swiper swiper-container-' . $custom_term->slug . '"><ul class="swiper-wrapper documentals-list">';
					while ( $loop->have_posts() ) : $loop->the_post();
						echo '<li class="swiper-slide documentals-modul" data-href="' . get_the_permalink() . '"><div class="content-swiper"><div class="linker"><p class="title">' . get_the_title() . '</p>';
						$subtitol = get_field( 'subtitol' );
						echo '<div class="subtitol">' . $subtitol . '</div><a href="' . get_the_permalink() . '" class="capitol-button">' . $veure_cap . '</a></div>';

						if ( get_field( 'imatge_capcalera' ) ) {
							$image = get_field( 'imatge_capcalera' );
							$size  = 'large';
							echo '<figure class="image">' . wp_get_attachment_image( $image, $size ) . ' ';
                            if ( get_field( 'peu_imatge_capcalera' ) ) {
                                $peu=get_field("peu_imatge_capcalera");
                            echo '<span class="credits-imatge">'. $peu .'</span></figure>';
                            }
                        }
						echo '</div></li>';

					endwhile;
					echo '</ul><div class="swiper-button swiper-button-next swiper-button-next-' . $custom_term->slug . '"></div><div class="swiper-button swiper-button-prev swiper-button-prev-' . $custom_term->slug . '"></div><div class="swiper-pagination-' . $custom_term->slug . '"></div>';
					echo '</div></div>';

				}
			}
			?>


        </div>
    </nav>
</header>
<div class="close-documental">
	<?php _e( 'Tancar', 'refugis' ); ?>
</div>


<!-- CONTENT DOCUEMENTAL -->

<article class="documental"></article>



