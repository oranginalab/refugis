<h2 class="section-title">
	<span><?php echo __("Crèdits","refugis"); ?></span>
</h2>

<div class="text-block">
<?php
	// We want to get the translated page by its slug
	$page = get_translated_page_by_slug('credits');
	// Display page contents
	if (is_object($page)){
		echo wp_filter_post_kses($page->post_content);
	}
	else {
		echo __('No hemos podido localizar el contenido de la página.');
	}
?>
    </div>
