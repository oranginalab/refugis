 


<div class="close"></div>
<header>
	<p class="title-web"><?php _e( "<span>La ciutat</span><span>dels</span><span>refugis</span>", "refugis" ); ?></p>
	<p class="sotstitol"><?php _e( "<span>Catàleg dels refugis</span><span> antiaeris de Barcelona</span>", "refugis" ); ?></p>
 
       <div class="language"><?php do_action('icl_language_selector'); ?></div>
    <p class="jump"><?php _e("Entrar","refugis"); ?></p>
       </div>
</header>
          
<div class="swiper-intro">
          
            <ul class="swiper-wrapper">
                  
                <!-- li class="swiper-slide back-100">
                        <span></span>
                  </li -->
                 <li class="swiper-slide back-90">
                    <div class="claim">
                       <p> “<?php _e("No vull infravalorar la severitat del càstig que cau damunt nostre, però confio que els nostres conciutadans seran capaços de resistir com va fer-ho el valent poble de Barcelona","refugis"); ?>.”</p>
                         <p class="firm">Winston Churchill. 1940</p>
                        
                        </div>
                    <span></span>
                  </li>
                
                
                <li class="swiper-slide back-80">
                     <div class="claim">
                       <p>“<?php _e("Quan als dos o tres mesos de guerra hom albirà la possibilitat que la capital fos bombardejada, el veïnat i l'Ajuntament començaren de prendre precaucions d'ordre elemental contra aquesta mena d'agressió","refugis"); ?>” 
                           </p>
                         <p class="firm"><?php _e("Manuel Muñoz Diez, conseller regidor d'Urbanització i Obres. Agost de 1937","refugis"); ?></p>
                        
                        </div>
                    <span></span>
                  </li>
                
                <li class="swiper-slide back-70">
                    <div class="claim">
                       <p> “<?php _e("Els terroristes de l'aire es vanen de llurs crims. Pretenen justificar la seva acció pretextant necessitat de guerra, objectius de guerra...","refugis"); ?> ”

                           </p>
                         <p class="firm"><?php _e('Pere Foix, "Les bombes de Barcelona", «Meridià», 25/02/1938','refugis'); ?> </p>
                        
                        </div>
                    <span></span>
                  </li>
                <li class="swiper-slide back-60">
                    <div class="claim">
                       <p> “<?php _e("S'esborraria el record del mal o el duria sempre amb mi com una malaltia de l'ànima?","refugis"); ?>”</p>
                         <p class="firm"><?php _e('Mercè Rodoreda, pròleg a "Quanta, quanta guerra...", 1980','refugis'); ?></p>
                        
                        </div>
                    <span></span>
                  </li>
                <li class="swiper-slide back-60">
                    <div class="claim">
                       <p> “<?php _e("El fang dels teus carrers, oh, Barcelona, és pastat amb sang","refugis"); ?>”
                           </p>
                         <p class="firm"><?php _e("La Humanitat, 23/03/1938","refugis"); ?></p>
                        
                        </div>
                    <span></span>
                  </li>
                <li class="swiper-slide back-50">
                    <div class="claim">
                       <p> “<?php _e("Els bombardeigs foren indiscriminats i sense cercar cap mena d'objectiu militar","refugis"); ?>”
                           </p>
                         <p class="firm"><?php _e('Joan Villarroya, "Els bombardeigs de Barcelona durant la Guerra Civil (1936-1939)", 1999','refugis'); ?></p>
                        
                        </div>
                    <span></span>
                  </li>
                <li class="swiper-slide back-50">
                    <div class="claim">
                       <p> “<?php _e("Potser el desig de llibertat en l'home és, més aviat, una necessitat de justícia.","refugis"); ?>”
                           </p>
                         <p class="firm"><?php _e('Mercè Rodoreda, pròleg a "Quanta, quanta guerra...", 1980','refugis'); ?></p>
                        
                        </div>
                    <span></span>
                  </li>
                <li class="swiper-slide back-40">
                    <div class="claim">
                       <p> “<?php _e("No se iniciará la construcción [de refugio] alguna sin la previa ejecución del plano facultativo de la obra a efectuar","refugis"); ?>”
                           </p>
                         <p class="firm"><?php _e("La Vanguardia, 15/02/1937","refugis"); ?></p>
                        
                        </div>
                    <span></span>
                  </li>
                <li class="swiper-slide back-40">
                    <div class="claim">
                       <p> “<?php _e("Malgrat les protestes internacionals, els bombardeigs damunt les poblacions de la rereguarda republicana no van aturar-se","refugis"); ?>”
                           </p>
                         <p class="firm"><?php _e('Joan Villarroya, "Els bombardeigs de Barcelona durant la Guerra Civil (1936-1939)", 1999','refugis'); ?></p>
                        
                        </div>
                    <span></span>
                  </li>
                <li class="swiper-slide back-30">
                    <div class="claim">
                       <p>  “<?php _e("Davant moltes botigues s'hi podien veure  llargues files de de dones amb vestits miserables i nens eixuts de carn","refugis"); ?>”
                           </p>
                         <p class="firm"><?php _e('Sygmunt Stein, "Brigades Internacionals. La fi d\'un mite", 2014','refugis'); ?></p>
                        
                        </div>
                    <span></span>
                  </li>
                <li class="swiper-slide back-30">
                    <div class="claim">
                       <p> “<?php _e("Encara no havien pogut començar a aixecar les runes de les cases per a treure els ferits o morts, que ja tornaven a comparèixer els avions","refugis"); ?>”
                           </p>
                         <p class="firm"><?php _e('Josefa Armangué, "Una família en exili. Memòries (1935-1965)", 1981',"refugis"); ?></p>
                        
                        </div>
                    <span></span>
                  </li>
                <li class="swiper-slide back-20">
                    <div class="claim">
                       <p> “<?php _e("Avui dia el front és la ciutat. El concepte d’urbicidi ens parla d’una altra faceta de la cultura de la guerra","refugis"); ?>”
                           </p>
                         <p class="firm"><?php _e('Antonio Monegal, Francesc Torres, José M. Ridao, "En guerra", 2004','refugis'); ?></p>
                        
                        </div>
                    <span></span>
                  </li>
                <li class="swiper-slide back-20">
                    <div class="claim">
                       <p> “<?php _e("¡No corregueu! ¡Quiets! ¡Estireu-vos a terra! El qui corre posa en greu perill la seva vida i assenyala a l’enemic la posició de les nostres forces.","refugis"); ?>”
                           </p>
                         <p class="firm"><?php _e("Cartell informatiu editat per la Generalitat de Catalunya ","refugis"); ?></p>
                        
                        </div>
                    <span></span>
                  </li>
                <li class="swiper-slide back-10">
                    <div class="claim">
                       <p>“<?php _e("I per tot Barcelona s’excavaven nous refugis subterranis, a prova de bomba.","refugis"); ?>”
                           </p>
                         <p class="firm"><?php _e('George Orwell. "Homage to Catalonia", 1938',"refugis"); ?></p>
                        
                        </div>
                    <span></span>
                  </li>
                <li class="swiper-slide back-10">
                    <div class="claim">
                       <p> “<?php _e("Voldria desitjar que no caiguessis malalt. Però quina malaltia hi ha més cruel que aquesta de la guerra?","refugis"); ?>”
                           </p>
                         <p class="firm"><?php _e('Màrius Torres, "Dietari per a en Victor", 1936','refugis'); ?></p>
                        
                        </div>
                    <span></span>
                  </li>
                </ul>
      <div class="swiper-pagination-detail"></div>
    </div>

                    
                    
                    
                    
                    <script>
                              
                        
            var swiper = new Swiper('.swiper-intro', {
                 autoplay: {
                    delay: 6000,
                    disableOnInteraction: false,
                     stopOnLastSlide:true,
                  },
                
                on: {
                    slideChange: function () {
                    
                    },
                    reachEnd: function() {
                     $(".intro,.global li").removeClass("active");
                     $(".cerca-value,.wpml-ls").addClass("active");
                     $("#brand").addClass("inactive");
                     $("h2.section-title.sobre_mapa").removeClass("hidden");
                     $(".contingut-proposta,.llista-propostes-descripcio, .llista-propostes, .proposal").removeClass("active");
                    },
                  },
                loop: true,
                 effect: 'fade',
                pagination: {
            el: '.swiper-pagination-detail',
             type: 'progressbar',
          },
            });
        </script>