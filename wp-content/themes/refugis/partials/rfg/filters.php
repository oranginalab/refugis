<!--
Filters block.
For styles: see wp-content/plugins/rfg/public/css/rfg-public.css
For Vue events see: wp-content/plugins/rfg/src/front/components/Filters.vue
-->
<script type="text/x-template" id="rfg-filters-template">

    <div id="rfg-filters">
        <h2 class="button-cerca">
            <span><?php _e( 'Explora el mapa', 'refugis' ); ?></span>
        </h2>
        <!-- Temporarily make plain Vue filters visible, just for reference
        <p>
            <label>Districtes:</label>
            <select v-model="filters.district">
                <option :value="null"> Tots els districtes </option>
                <option v-for="(district, index) in districts" :key="index" :value="district">{{ district }}
                </option>
            </select>
            <label>Categories:</label>
            <select v-model="filters.category">
                <option :value="null"> Totes les categories </option>
                <option v-for="(category, index) in categories" :key="index" :value="category">{{ category }}
                </option>
            </select>
        </p>
        <p>
            <label>Address:</label>
            <input v-model="filters.address"/>
            <label>Visitable:</label>
            <select v-model="filters.visitable">
                <option :value="null"> No filter </option>
                <option :value="1">Yes</option>
                <option :value="0">No</option>
            </select>
            <button @click="filter">Filter</button>
        </p -->

        <!-- *** DAVID's Filtres *** -->
        <div class="cercador">
            <!-- Search error, no results or API error response. -->
            <div class="search-no-results error" v-if="message">{{ message }}</div>
            <form @submit.prevent="filter">
                <div class="anys">
                    <p id="labels-years-output"
                       class="titol-opcio-cerca"><?php _e( 'Sel·lecciona l\'any de construcció', 'refugis' ); ?></p>
                    <div class="masq-slider">
                        <div class="slider"></div>
                    </div>

                </div>
                <div class="tipologies tipos">

                    <p class="titol-opcio-cerca"><?php _e( 'Sel·lecciona una tipologia', 'refugis' ); ?></p>
                    <div class="expand-tipologies">
                        <ul>
                            <li>
                                <a href="javascript:void(0)"
                                   @click="filterUpdated('category', null, '<?php _e( 'Totes', 'refugis' ); ?>')"><?php _e( 'Totes', 'refugis' ); ?></a>
                            </li>
                            <li v-for="(category, index) in categories" :key="index">
                                <a href="javascript:void(0)" @click="filterUpdated('category', category, category)">{{
                                    category }}</a>
                            </li>
                        </ul>
                        <p class="tipologia-active data-category-active"><?php _e( 'Totes', 'refugis' ); ?></p>
                    </div>

                </div>
                <div class="tipologies districtes">

                    <p class="titol-opcio-cerca"><?php _e( 'Sel·lecciona un districte', 'refugis' ); ?></p>
                    <div class="expand-tipologies">
                        <ul>
                            <li>
                                <a href="javascript:void(0)"
                                   @click="filterUpdated('district', null, '<?php _e( 'Tots', 'refugis' ); ?>')"><?php _e( 'Tots', 'refugis' ); ?></a>
                            </li>
                            <li v-for="(district, index) in districts" :key="index">
                                <a href="javascript:void(0)" @click="filterUpdated('district', district, district)">{{
                                    district }}</a>
                            </li>
                        </ul>
                        <p class="tipologia-active data-district-active"><?php _e( 'Totes', 'refugis' ); ?></p>
                    </div>

                </div>
                <div class="free-cerca">
                    <p class="titol-opcio-cerca"><?php _e( 'Cerca lliure', 'refugis' ); ?></p><input class="input-cerca"
                                                                                                     v-model="filters.address"
                                                                                                     placeholder="<?php _e( 'Cerca per carrer o nom de refugi..', 'refugis' ); ?>">

                </div>
                <p class="buttons"><input placeholder="Cerca" type="button" value="cercar" @click="filter"></p>
            </form>
        </div>


    </div>
</script>
