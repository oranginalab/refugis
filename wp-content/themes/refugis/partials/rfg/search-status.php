<script type="text/x-template" id="rfg-search-status-template">
    <div id="rfg-search-status" :class="{active: show}">
        <p class="search-hits" v-show="hits > 0">
            <span class="search-hits-value">{{ hits }} Refugis</span>
        </p>

        <ul class="search-status-details">
            <li v-if="query.chronology"><span class="search-filter-value">{{ query.chronology[0] }}</span> -
                <span class="search-filter-value">{{ query.chronology[1] }}</span></li>
            <li v-if="query.group"><span class="search-filter-value">{{ query.group }}</span></li>
            <li v-if="query.category"><span class="search-filter-value">{{ query.category }}</span></li>
            <li v-if="query.district"><span class="search-filter-value">{{ query.district }}</span></li>
            <li v-if="query.address"><span class="search-filter-value">{{ query.address }}</span></li>
        </ul>

        <button class="search-reset" @click="resetFilters">Borrar</button>
    </div>
</script>
