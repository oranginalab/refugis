<?php
/** @var $api_data - raw data received from shelter API */

/** @var $details - nested shelter details data */

// api_data fields

/**
 * @param $data
 * @param $path
 *
 * @return bool|string|array
 */
function getPropValue( $data, $path ) {
	$path_arr = is_array( $path ) ? $path : explode( '.', $path );
	if ( is_array( $path_arr ) && ! empty( $path_arr ) ) {
		// We support $path suffix '[]' to return an array of values.
		if ( substr( end( $path_arr ), - 2 ) == '[]' ) {
			array_push( $path_arr, substr( array_pop( $path_arr ), 0, - 2 ) );
			$return_array = true;
		} else {
			$return_array = false;
		}
		$node = array_reduce( $path_arr, function ( $carry, $item ) use ( $return_array ) {
			if ( isset( $carry->{$item} ) ) {
				if ( is_array( $carry->{$item} ) ) {
					// If we expect an array of values just return it, otherwise pick the first element.
					return ! empty( $return_array ) ? $carry->{$item} : $carry->{$item}[0];
				}
				if ( is_object( $carry->{$item} )
				     // If value != 'Si', discard children even if set.
				     && isset( $carry->{$item}->value )
				     && trim( $carry->{$item}->value ) == 'Si'
				     && isset( $carry->{$item}->children ) ) {
					return $carry->{$item}->children;
				}

				// Plain item.
				return $carry->{$item};
			}

			return false;
		}, $data );

		if ( $return_array && ! is_array( $node ) ) {
			// If we expect an array of values and we got a single item, convert it to
			// single-element array.
			$node = [ $node ];
		}

		if ( isset( $node->value ) ) {
			return trim( $node->value );
		} else {
			if ( is_object( $node ) ) {
				// Scan nested nodes recursively.
				$result = [];
				foreach ( $node as $k => $v ) {
					$path_nested  = array_merge( $path_arr, [ $k ] );
					$result[ $k ] = getPropValue( $data, $path_nested );
				}

				return $result;
			} elseif ( is_array( $node ) ) {
				// Node is an array, pick values.
				$array = [];
				foreach ( $node as $key => $value ) {
					if ( isset( $value->value ) ) {
						// Plain value here.
						array_push( $array, $value->value );
					} elseif ( is_object( $value ) ) {
						// Nested object - create an array recursively scanning nodes.
						// TODO: looks like code duplication - maybe we can optimize it.
						$array[ $key ] = [];
						foreach ( $value as $k => $v ) {
							$path_nested         = array_merge( $path_arr, [ $k ] );
							$array[ $key ][ $k ] = getPropValue( $data, $path_nested );
						}
					}
				}

				return $array;
			}
		}

		return false;
	}

	return false;
}


// Other properties.
$tipologia       = getPropValue( $details, 'TIPOLOGIA.Tipologia' );
$nom             = $api_data->name;
$nom_singular    = getPropValue( $details, 'NOM.Nom Singular' );
$any_construccio = getPropValue( $details, 'CRONOLOGIA.Construcció.Inici Construcció.Primer Any Conegut' );


// ADREÇA
$adreca_actual_adreca    = getPropValue( $details, 'SITUACIÓ.Adreça Actual.Adreça' );
$adreca_actual_districte = getPropValue( $details, 'SITUACIÓ.Adreça Actual.Districte' );
$adreca_actual           = $adreca_actual_adreca;
if ( $adreca_actual_districte ):
	$adreca_actual .= ' - ' . $adreca_actual_districte;
endif;

$adreca_1937_adreca    = getPropValue( $details, 'SITUACIÓ.Adreça Històrica.Adreça Cens 1937.Adreça' );
$adreca_1937_districte = getPropValue( $details, 'SITUACIÓ.Adreça Històrica.Adreça Cens 1937.Districte' );
$adreca_1937           = $adreca_1937_adreca;
if ( $adreca_1937_districte ):
	$adreca_1937 .= ' - ' . $adreca_1937_districte;
endif;

$adreca_1938_adreca    = getPropValue( $details, 'SITUACIÓ.Adreça Històrica.Adreça Cens 1938.Adreça' );
$adreca_1938_districte = getPropValue( $details, 'SITUACIÓ.Adreça Històrica.Adreça Cens 1938.Districte' );
$adreca_1938           = $adreca_1938_adreca;
if ( $adreca_1938_districte ):
	$adreca_1938 .= ' - ' . $adreca_1938_districte;
endif;

$adreca_1943_adreca    = getPropValue( $details, 'SITUACIÓ.Adreça Històrica.Adreça Cens 1943.Adreça' );
$adreca_1943_districte = getPropValue( $details, 'SITUACIÓ.Adreça Històrica.Adreça Cens 1943.Districte' );
$adreca_1943           = $adreca_1943_adreca;
if ( $adreca_1943_districte ):
	$adreca_1943 .= ' - ' . $adreca_1943_districte;
endif;

$localitzacio = getPropValue( $details, 'SITUACIÓ.Localització.Localització' );

// DESCRIPCIÓ
$descripcio       = getPropValue( $details, 'DESCRIPCIÓ.Descripció' );
$fons_financament = getPropValue( $details, 'DESCRIPCIÓ.Fons de Finançament.Fons de Finançament' );

// CRONOLOGIA

$cronologia_construccio_inici_data = getPropValue( $details, 'CRONOLOGIA.Construcció.Inici Construcció.Data' );
$cronologia_construccio_inici_any  = getPropValue( $details, 'CRONOLOGIA.Construcció.Inici Construcció.Primer Any Conegut' );
$cronologia_construccio_inici      = ( $cronologia_construccio_inici_data ) ? $cronologia_construccio_inici_data : $cronologia_construccio_inici_any;

$cronologia_construccio_final_data = getPropValue( $details, 'CRONOLOGIA.Construcció.Final Construcció.Any Final Construcció' );
$cronologia_construccio_final_any  = getPropValue( $details, 'CRONOLOGIA.Construcció.Final Construcció.Primer Any Conegut' );
$cronologia_construccio_final      = ( $cronologia_construccio_final_data ) ? $cronologia_construccio_final_data : $cronologia_construccio_final_any;

$cronologia_construccio = '';
if ( $cronologia_construccio_inici && $cronologia_construccio_final ) {
	$cronologia_construccio = $cronologia_construccio_inici . ' / ' . $cronologia_construccio_final;
}


$cronologia_clausura_data = getPropValue( $details, 'CRONOLOGIA.Data de la Clausura.Any de la Clausura' );
$cronologia_clausura_any  = getPropValue( $details, 'CRONOLOGIA.Data de la Clausura.Data' );
$cronologia_clausura      = ( $cronologia_clausura_data ) ? $cronologia_clausura_data : $cronologia_clausura_any;

$cronologia_us_data      = getPropValue( $details, 'CRONOLOGIA.Ús Secundari.Data de l\'ús' );
$cronologia_us_tipologia = getPropValue( $details, 'CRONOLOGIA.Ús Secundari.Tipologia de l\'ús' );
$cronologia_us = '';
if ( $cronologia_us_tipologia ) {
	$cronologia_us = $cronologia_us_tipologia;
	if ( $cronologia_us_data ) {
		$cronologia_us .= ' (' . $cronologia_us_data . ')';
	}
}

// TIPOLOGIA
$tipologia = getPropValue( $details, 'TIPOLOGIA.Tipologia' );
$cabuda    = getPropValue( $details, 'TIPOLOGIA.Número Cabuda ' );


// CONSERVACIO ACTUAL
$conservacio_desconegut = getPropValue( $details, 'CONSERVACIÓ ACTUAL.Desconegut' );
$conservacio_desconegut = ( $conservacio_desconegut === 'Sí' );

$conservacio_estat        = getPropValue( $details, 'CONSERVACIÓ ACTUAL.Conegut.Estat De Conservació' );
$conservacio_estat_actual = getPropValue( $details, 'CONSERVACIÓ ACTUAL.Conegut.Estat Actual' );
$conservacio_descripcio   = getPropValue( $details, 'CONSERVACIÓ ACTUAL.Conegut.Descripció' );

// INFORMACIÓ TÈCNICA
$construit             = getPropValue( $details, 'INFORMACIÓ TÈCNICA I LEGAL.Característiques Tècniques.Construït' );
$dimensions            = getPropValue( $details, 'INFORMACIÓ TÈCNICA I LEGAL.Característiques Tècniques.Dimensions' );
$cotes_profunditat     = getPropValue( $details, 'INFORMACIÓ TÈCNICA I LEGAL.Característiques Tècniques.Cotes / Profunditat' );
$entrades              = getPropValue( $details, 'INFORMACIÓ TÈCNICA I LEGAL.Característiques Tècniques.Entrades' );
$elements_constructius = getPropValue( $details, 'INFORMACIÓ TÈCNICA I LEGAL.Característiques Tècniques.Elements Constructius.Elements' );
$illuminacio           = getPropValue( $details, 'INFORMACIÓ TÈCNICA I LEGAL.Característiques Tècniques.Il·luminació.Il·luminació' );

$grafits      = getPropValue( $details, 'INFORMACIÓ TÈCNICA I LEGAL.Característiques Tècniques.Grafits.Grafits' );
$grafits_desc = getPropValue( $details, 'INFORMACIÓ TÈCNICA I LEGAL.Característiques Tècniques.Grafits.Descripció' );
$grafits      = ( $grafits_desc ) ? $grafits_desc : $grafits;


// DOCUMENTACIÓ
$documentation = getPropValue( $details, 'DOCUMENTACIÓ EXISTENT' );

// "DOCUMENTACIÓ EXISTENT" section data.
$documentacio_existent = array_intersect_key( $documentation, array_flip( [
	'Llistat 9 de Desembre de 1937',
	'Llistat 16 de Juliol de 1938',
	'Plànol Inventari de 1962',
] ) );
// Only show valid entries.
$documentacio_existent = array_filter( $documentacio_existent, function ( $item ) {
	return $item == 'Si';
} );

$planol_1969     = getPropValue( $details, 'DOCUMENTACIÓ EXISTENT.Plànol Anys 1969-1973.Referència' );
$planol_original = getPropValue( $details, 'DOCUMENTACIÓ EXISTENT.Plànol Original.Referència' );

$documentacio_fotogràfica = getPropValue( $details, 'DOCUMENTACIÓ EXISTENT.Documentació Fotogràfica.Referència' );
$documentacio_escrita     = getPropValue( $details, 'DOCUMENTACIÓ EXISTENT.Documentació escrita.Referència' );
$documentacio_oral        = getPropValue( $details, 'DOCUMENTACIÓ EXISTENT.Documentació Oral.Referència' );


// "Documentació Arqueològica" section data.
$has_documentacio_arqueologica = ( getPropValue( $details, 'DOCUMENTACIÓ EXISTENT.Documentació Arqueològica' ) === 'Sí' );

// "Referència Carta Arqueològica" section data. *** We expect and array ov values here.
$referencia_carta_arquelogica = getPropValue( $details, 'DOCUMENTACIÓ EXISTENT.Documentació Arqueològica.Referència Carta Arqueològica[]' );
$escaner_3D                   = getPropValue( $details, 'DOCUMENTACIÓ EXISTENT.Documentació Arqueològica.Escàner 3D' );

// "DOCUMENTACIÓ ENLLAÇADA" section data. *** We expect an array of attached documents.
$documentacio_adjunta = getPropValue( $details, 'DOCUMENTACIÓ EXISTENT.DOCUMENTACIÓ ENLLAÇADA[]' );
// Check that we have all required properties.
$documentacio_adjunta = array_filter( $documentacio_adjunta, function ( $doc ) {
	return isset( $doc['Tipus Document'] )
	       && isset( $doc['Nom Document'] )
	       && isset( $doc['Enllaç / URL'] )
	       && isset( $doc['Copyright / Referència'] )
	       && isset( $doc['Descripció'] );
} );
// Set DOM class depending on document type.
$documentacio_adjunta = array_map( function ( $doc ) {
	return array_merge( $doc, [
		'class' => substr( $doc['Tipus Document'], 0, strlen( 'Imatge' ) ) == 'Imatge'
			? 'imatge' : 'document',
	] );
}, $documentacio_adjunta );

// Main documents: First level documents (newly added to API contract)
$main_documents = isset($api_data->documents) ? $api_data->documents : null;

$photos = $api_data->photos;


// wp fields
if ( isset( $api_data->wp_post ) ):
	// Post data shortcuts.
	global $post;
	$post          = $api_data->wp_post;
	$custom_fields = $post->meta;

	// Custom fields
	$video_testimoni       = esc_html( $custom_fields[ RFG_SHELTER_VIDEO_TESTIMONIAL_META_KEY ] );
	$video_testimoni_peu   = esc_html( $custom_fields[ RFG_SHELTER_VIDEO_TESTIMONIAL_PEU_META_KEY ] );
	$video_tour            = esc_html( $custom_fields[ RFG_SHELTER_VIDEO_TOUR_META_KEY ] );
	$video_tour_peu        = esc_html( $custom_fields[ RFG_SHELTER_VIDEO_TOUR_PEU_META_KEY ] );
	$carta_arqueologica_id = esc_html( $custom_fields[ RFG_SHELTER_ID_CARTA_META_KEY ] );

endif;
?>
<style>
    .ext-json {
        color: darkred !important;
    }
</style>

<div class="close" id="shelter-details-close-btn"></div>

<article class="detail-fitxa">
    <header>
        <div class="title-fitxa">
            <h3 class="title"> <?php _e( 'Refugi', 'refugis' ); ?>: <?php echo esc_html( $nom ); ?></h3>
            <p class="construccio"><?php echo esc_html( $nom_singular ); ?></p>
        </div>
        <div class="sobretaula">
        <?php
            if (!empty(array_filter([
                    $adreca_actual,
                    $adreca_1937,
                    $adreca_1938,
                    $adreca_1943,
                    $localitzacio
            ]))):
        ?>
            <div class="adreca info-tech">
                <h4><?php echo _e( 'Adreça', 'refugis' ); ?></h4>
                <dl>
                    <?php
                    if ( $adreca_actual ):
                        ?>
                        <dt><?php echo _e( 'Adreça actual', 'refugis' ); ?></dt>
                        <dd><?php echo esc_html( $adreca_actual ); ?></dd>
                    <?php
                    endif;

                    if ( $adreca_1937 ):
                        ?>
                        <dt><?php echo _e( 'Adreça Cens 1937', 'refugis' ); ?></dt>
                        <dd><?php echo esc_html( $adreca_1937 ); ?></dd>
                    <?php
                    endif;

                    if ( $adreca_1938 ):
                        ?>
                        <dt><?php echo _e( 'Adreça Cens 1938', 'refugis' ); ?></dt>
                        <dd><?php echo esc_html( $adreca_1938 ); ?></dd>
                    <?php
                    endif;

                    if ( $adreca_1943 ):
                        ?>
                        <dt><?php echo _e( 'Adreça Cens 1943', 'refugis' ); ?></dt>
                        <dd><?php echo esc_html( $adreca_1943 ); ?></dd>
                    <?php
                    endif;

                    if ( $localitzacio ):
                        ?>
                        <dt><?php echo _e( 'Localitzacio', 'refugis' ); ?></dt>
                        <dd><?php echo esc_html( $localitzacio ); ?></dd>
                    <?php
                    endif;

                    ?>
                </dl>
            </div>
        <?php
            endif;
        ?>
            </div>
    </header>

    <div class="detail-fitxa-tecnica">

        
        <div class="mobil">
        <?php
            if (!empty(array_filter([
                    $adreca_actual,
                    $adreca_1937,
                    $adreca_1938,
                    $adreca_1943,
                    $localitzacio
            ]))):
        ?>
            <div class="adreca info-tech">
                <h4><?php echo _e( 'Adreça', 'refugis' ); ?></h4>
                <dl>
                    <?php
                    if ( $adreca_actual ):
                        ?>
                        <dt><?php echo _e( 'Adreça actual', 'refugis' ); ?></dt>
                        <dd><?php echo esc_html( $adreca_actual ); ?></dd>
                    <?php
                    endif;

                    if ( $adreca_1937 ):
                        ?>
                        <dt><?php echo _e( 'Adreça Cens 1937', 'refugis' ); ?></dt>
                        <dd><?php echo esc_html( $adreca_1937 ); ?></dd>
                    <?php
                    endif;

                    if ( $adreca_1938 ):
                        ?>
                        <dt><?php echo _e( 'Adreça Cens 1938', 'refugis' ); ?></dt>
                        <dd><?php echo esc_html( $adreca_1938 ); ?></dd>
                    <?php
                    endif;

                    if ( $adreca_1943 ):
                        ?>
                        <dt><?php echo _e( 'Adreça Cens 1943', 'refugis' ); ?></dt>
                        <dd><?php echo esc_html( $adreca_1943 ); ?></dd>
                    <?php
                    endif;

                    if ( $localitzacio ):
                        ?>
                        <dt><?php echo _e( 'Localitzacio', 'refugis' ); ?></dt>
                        <dd><?php echo esc_html( $localitzacio ); ?></dd>
                    <?php
                    endif;

                    ?>
                </dl>
            </div>
        <?php
            endif;
        ?>
            </div>
        <?php

            if (!empty(array_filter([
                $descripcio,
                $fons_financament,
            ]))):
	    ?>
            <div class="descripcio info-tech">
                <h4><?php echo _e( 'Descripció', 'refugis' ); ?></h4>
                <dl>
                    <?php
                    if ( $descripcio ):
                        ?>
                        <dt><?php echo _e( 'Descripció', 'refugis' ); ?></dt>
                        <dd><?php echo wp_kses( $descripcio, [ 'br' => [] ] ); ?></dd>
                    <?php
                    endif;

                    if ( $fons_financament ):
                        ?>
                        <dt><?php echo _e( 'Fons de finançament', 'refugis' ); ?></dt>
                        <dd><?php echo esc_html( $fons_financament ); ?></dd>
                    <?php
                    endif;

                    ?>
                </dl>
            </div>
        <?php
            endif;

            if (!empty(array_filter([
                $cronologia_construccio,
                $cronologia_construccio_inici,
                $cronologia_construccio_inici,
                $cronologia_construccio_final,
                $cronologia_clausura,
                $cronologia_us
            ]))):
        ?>

            <div class="cronologia info-tech">
                <h4><?php echo _e( 'Cronologia', 'refugis' ); ?></h4>
                <dl>
                    <?php
                    if ( ! empty( $cronologia_construccio ) ):
                        ?>
                        <dt><?php echo _e( 'Construcció', 'refugis' ); ?></dt>
                        <dd><?php echo esc_html( $cronologia_construccio ); ?></dd>
                    <?php
                    elseif ( ! empty( $cronologia_construccio_inici ) ):
                        ?>
                        <dt><?php echo _e( 'Inici construcció', 'refugis' ); ?></dt>
                        <dd><?php echo esc_html( $cronologia_construccio_inici ); ?></dd>
                    <?php
                    elseif ( ! empty( $cronologia_construccio_final ) ):
                        ?>
                        <dt><?php echo _e( 'Final Construcció', 'refugis' ); ?></dt>
                        <dd><?php echo esc_html( $cronologia_construccio_final ); ?></dd>
                    <?php
                    endif;

                    if ( $cronologia_clausura ):
                        ?>
                        <dt><?php echo _e( 'Data de la clausura', 'refugis' ); ?></dt>
                        <dd><?php echo esc_html( $cronologia_clausura ); ?></dd>
                    <?php
                    endif;

                    if ( ! empty( $cronologia_us ) ):
                        ?>
                        <dt><?php echo _e( 'Ús secundari', 'refugis' ); ?></dt>
                        <dd><?php echo esc_html( $cronologia_us ); ?></dd>
                    <?php
                    endif;

                    ?>
                </dl>
            </div>
        <?php
            endif;

            if (!empty(array_filter([
                $tipologia,
                $cabuda,
            ]))):
        ?>

            <div class="tipologia info-tech">
                <h4><?php echo _e( 'Tipologia', 'refugis' ); ?></h4>
                <dl>
                    <?php
                    if ( $tipologia ):
                        ?>
                        <dt><?php echo _e( 'Tipologia', 'refugis' ); ?></dt>
                        <dd><?php echo esc_html( $tipologia ); ?></dd>
                    <?php
                    endif;

                    if ( $cabuda ):
                        ?>
                        <dt><?php echo _e( 'Cabuda', 'refugis' ); ?></dt>
                        <dd><?php echo esc_html( $cabuda ); ?></dd>
                    <?php
                    endif;

                    ?>
                </dl>
            </div>

        <?php
            endif;

            if (!empty(array_filter([
	            $conservacio_desconegut,
	            $conservacio_estat,
	            $conservacio_estat_actual,
	            $conservacio_descripcio
            ]))):
        ?>

            <div class="conservacio info-tech">
                <h4><?php echo _e( 'Conservació actual', 'refugis' ); ?></h4>
                <dl>
                    <?php
                    if ( $conservacio_desconegut ):
                        ?>
                        <dt><?php echo _e( 'Desconegut', 'refugis' ); ?></dt>
                        <dd><?php echo _e( 'Sí', 'refugis' ); ?></dd>
                    <?php
                    else:
                        if ( $conservacio_estat ):
                            ?>
                            <dt><?php echo _e( 'Estat de conservació', 'refugis' ); ?></dt>
                            <dd><?php echo esc_html( $conservacio_estat ); ?></dd>
                        <?php
                        endif;

                        if ( $conservacio_estat_actual ):
                            ?>
                            <dt><?php echo _e( 'Estat actual', 'refugis' ); ?></dt>
                            <dd><?php echo esc_html( $conservacio_estat_actual ); ?></dd>
                        <?php
                        endif;

                        if ( $conservacio_descripcio ):
                            ?>
                            <dt><?php echo _e( 'Descripció', 'refugis' ); ?></dt>
                            <dd><?php echo esc_html( $conservacio_descripcio ); ?></dd>
                        <?php
                        endif;
                        ?>
                    <?php
                    endif;

                    ?>
                </dl>
            </div>

        <?php
        endif;

        if (!empty(array_filter([
	        $construit,
	        $dimensions,
	        $cotes_profunditat,
	        $entrades,
	        $elements_constructius,
	        $illuminacio,
	        $grafits,
        ]))):
        ?>
            <div class="info-tech">
                <h4><?php echo _e( 'Informació tècnica', '' ); ?></h4>
                <dl>
                    <?php
                    if ( $construit ):;
                        ?>
                        <dt><?php _e( "Construit", "refugis" ) ?></dt>
                        <dd><?php echo esc_html( $construit ); ?> </dd>
                    <?php
                    endif;

                    if ( $dimensions ): ?>
                        <dt><?php _e( 'Dimensions', 'refugis' ); ?></dt>
                        <dd><?= esc_html( $dimensions ) ?></dd>
                    <?php endif; ?>
                    <?php if ( $cotes_profunditat ): ?>
                        <dt><?php _e( 'Cotes / Profunditat', 'refugis' ); ?></dt>
                        <dd><?= esc_html( $cotes_profunditat ) ?></dd>
                    <?php endif; ?>
                    <?php if ( $entrades ): ?>
                        <dt><?php _e( 'Entrades', 'refugis' ); ?></dt>
                        <dd><?= esc_html( $entrades ) ?></dd>
                    <?php endif; ?>

                    <?php
                    if ( $elements_constructius ):
                        ?>
                        <dt><?php _e( "Elements constructius", "refugis" ) ?></dt>
                        <dd><?php echo esc_html( $elements_constructius ); ?> </dd>
                    <?php
                    endif;

                    if ( $illuminacio ): ?>
                        <dt><?php _e( 'Il·luminació', 'refugis' ); ?></dt>
                        <dd><?= esc_html( $illuminacio ) ?></dd>
                    <?php endif; ?>

                    <?php if ( $grafits ): ?>
                        <dt><?php _e( 'Grafits', 'refugis' ); ?></dt>
                        <dd><?= esc_html( $grafits ) ?></dd>
                    <?php endif; ?>
                </dl>
            </div>
        <?php
            endif;

            if (!empty(array_filter([
                $documentation,
                $documentacio_existent,
                $planol_1969,
                $planol_original,
                $documentacio_fotogràfica,
                $documentacio_escrita,
                $documentacio_oral,
                $has_documentacio_arqueologica,
                $documentacio_adjunta,
                $main_documents
            ]))):
        ?>
            <div class="documentacio info-tech">
                <!-- Amb el JSON es va construint la fitxa tal que: -->
                <?php if ( !empty($documentation) ): ?>
                    <h4>Documentació</h4>
                    <dl>
                        <?php if ( !empty($documentacio_existent) || !empty($main_documents) ): ?>
                            <dt>Documentació existent</dt>

                            <!-- Main documentation -->
                            <?php foreach ($main_documents as $main_document): ?>
                                <dd><a href="<?php echo RFG_SHELTERS_DOC_BASE_URL . $main_document->url; ?>" target="_blank"><?php echo $main_document->nom; ?></a></dd>
                            <?php endforeach; ?>

                            <!-- Documentation list -->
                            <?php foreach ( array_keys( $documentacio_existent ) as $key ): ?>
                                <dd><?php _e( $key, 'refugis' ) ?> </dd>
                            <?php endforeach; ?>
                        <?php endif; ?>


                        <?php if ( $planol_1969 ): ?>
                            <dt><?php _e( "Plànol Anys 1969-1973", "refugis" ) ?></dt>
                            <dd>
                                <?php echo esc_html( $planol_1969 ) ?>
                            </dd>
                        <?php endif; ?>

                        <?php if ( $planol_original ): ?>
                            <dt><?php _e( 'Plànol original', 'refugis' ); ?></dt>
                            <dd>
                                <?php echo esc_html( $planol_original ) ?>
                            </dd>
                        <?php endif; ?>

                        <?php if ( $documentacio_fotogràfica ): ?>
                            <dt><?php _e( 'Documentació fotogràfica', 'refugis' ); ?></dt>
                            <dd>
                                <?php echo esc_html( $documentacio_fotogràfica ) ?>
                            </dd>
                        <?php endif; ?>

                        <?php if ( $documentacio_escrita ): ?>
                            <dt><?php _e( 'Documentació escrita', 'refugis' ); ?></dt>
                            <dd>
                                <?php echo esc_html( $documentacio_escrita ) ?>
                            </dd>
                        <?php endif; ?>

                        <?php if ( $documentacio_oral ): ?>
                            <dt><?php _e( 'Documentació oral', 'refugis' ); ?></dt>
                            <dd>
                                <?php echo esc_html( $documentacio_oral ) ?>
                            </dd>
                        <?php endif; ?>

                        <?php
                        if ( $has_documentacio_arqueologica ):

                            if ( $referencia_carta_arquelogica ): ?>
                                <dt><?php _e( 'Referència Carta Arqueoloògica', 'refugis' ); ?></dt>

                                <?php foreach ( $referencia_carta_arquelogica as $ref ): ?>
                                    <dd>
                                        <a href="<?php echo esc_url( $ref ); ?>"><?php echo esc_html( $ref ); ?></a>
                                    </dd>
                                <?php endforeach; ?>
                            <?php endif; ?>

                            <?php if ( $escaner_3D ): ?>
                            <dt><?php _e( 'Escàner 3D', 'refugis' ); ?></dt>
                            <dd>
                                <?php echo esc_html( $escaner_3D ) ?>
                            </dd>
                        <?php endif;

                        endif;
                        ?>

                        <?php if ( $documentacio_adjunta ): ?>
                            <dt><?php _e( 'Documentació adjunta', 'refugis' ); ?></dt>
                            <?php foreach ( $documentacio_adjunta as $doc ): ?>
                                <dd class="<?= $doc['class'] ?>">
                                    <a href="<?= esc_url( $doc['Enllaç / URL'] ) ?>"><?= esc_html( $doc['Nom Document'] ) ?></a><br>
                                    <span class="desc-doc-adj"><?= esc_html( $doc['Descripció'] ) ?></span><br>
                                    <span class="copyr"><?= esc_html( $doc['Copyright / Referència'] ) ?></span>
                                </dd>
                            <?php endforeach; ?>
                        <?php endif; ?>
                    </dl>
                <?php endif; ?>
            </div>
        <?php
        endif;
        ?>

    </div>

    <!-- JSON: IMATGES -->
    <div class="detail-images">

		<?php if ( $photos ): ?>
            <h4><?php _e( "En imatges", "refugis" ) ?></h4>

            <div class="swiper-images">

                <ul class="swiper-wrapper" style="color:black;"><!-- Override default intro swiper color. -->
					<?php foreach ( $photos as $photo ): ?>
                        <li class="swiper-slide">
                            <p class="image"><img alt="<?= $photo->Title ?>"
                                                  src="<?= esc_url( $photo->Url ) ?>">
                            </p>
                            <p class="footer-image"><?= esc_html( $photo->Description ) ?></p>
                        </li>
					<?php endforeach; ?>
                </ul>

                <div class="swiper-images-pagination-detail"></div>
            </div>
		<?php endif; ?>
    </div>


    <!-- CUSTOM FIELDS: VIDEOS -->
    <div class="detail-descripcio">

		<?php if ( ! empty( $video_testimoni ) ): ?>

            <div class="embed-container">
                <h4><?php _e( "Testimonis", "refugis" ) ?></h4>
                <?php
				// Load value.

				// Add extra parameters to src.
				$params = [
					'controls'   => 0,
					'hd'         => 1,
					'title'      => 0,
					'background' => 1,
					'autohide'   => 1,
					'wmode'      => 'opaque',
				];
				$src    = add_query_arg( $params, $video_testimoni );

				$attributes = 'frameborder="0" allowfullscreen';

				$iframe = '<iframe src="' . $src . '" ' . $attributes . ' ></iframe>';

				echo $iframe;
				?>
                <!-- p class="peu-video"> Captar valor ACF peu_video_testimoni </p --> <!-- Afegir if (! empty()); -->
                
                <?php
			if ( ! empty( $video_testimoni_peu ) ):
				?>
                <p class="video_peu"><?php echo $video_testimoni_peu; ?></p>
			<?php
			endif;
			?>
            </div>
			
		<?php endif; ?>

		<?php if ( ! empty( $video_tour ) ): ?>

            <div class="embed-container">
                <h4><?php _e( "Tour virtual", "refugis" ) ?></h4>
                <?php
				// Load value.

				// Add extra parameters to src.
				$params = [
					'controls'   => 0,
					'hd'         => 1,
					'title'      => 0,
					'background' => 1,
					'autohide'   => 1,
					'wmode'      => 'opaque',
				];
				$src    = add_query_arg( $params, $video_tour );

				$attributes = 'frameborder="0" allowfullscreen';

				$iframe = '<iframe src="' . $src . '" ' . $attributes . '></iframe>';

				echo $iframe;
				?>
                <!-- p class="peu-video"> Captar valor ACF peu_video_testimoni </p --> <!-- Afegir if (! empty()); -->
                
                	<?php
			if ( ! empty( $video_testimoni_peu ) ):
				?>
                <p class="video_peu"><?php echo $video_testimoni_peu; ?></p>
			<?php
			endif;
			?>
            </div>
		
		<?php endif; ?>





    <!-- CUSTOM FIELD: PLANIMETRIA -->
	<?php if ( have_rows( 'planimetria' ) ): ?>
             <div class="embed-container">
                <div class="planimetria">
                    <ul>
                        <?php while ( have_rows( 'planimetria' ) ): the_row();
                            $map_img = get_sub_field( 'imatge_mapa' );
                            $url     = $map_img['url'];

                            $footer = get_sub_field( 'peu_planimetria' );
                            ?>

                            <li>
                                <img src="<?php echo $url; ?>"/>
                                <p class="imatge_peu"><?php echo $footer; ?></p>
                            </li>
                        <?php endwhile; ?>
                    </ul>
                  </div>
             </div>
	<?php endif; ?>





    </div>


    <!-- CUSTOM FIELD: CARTA ARQUEOLÒGICA ID -->
	<?php
	if ( ! empty( $carta_arqueologica_id ) ):
		?>
        <dl class="carta_arqueologica_id">
            <dd><?php _e( 'Carta Arqueològica ID', 'refugis' ); ?></dd>
            <dt><?php echo $carta_arqueologica_id; ?></dt>
        </dl>
	<?php
	endif;
	?>


    <!--
		A quick link to edit related WP post. We show it in DEBUG mode, later we can refactor and create a new define,
		e.g. WP_STAGING, so that content-responsible people can easily update WP post clicking linked shelter feature
		on map.
		-->

	<?php /* if ( defined( 'WP_DEBUG' ) && true === WP_DEBUG && isset( $post ) ): ?>
        <a href="<?=admin_url()?>post.php?post=<?=$post->ID?>&action=edit">Edit WP <?=RFG_SHELTER_POST_TYPE?> post</a>
	<?php endif;  */ ?>

</article>


	<?php // if ( isset( $post ) && $post->post_status == 'publish' ): ?>
            <!-- Social sharing -->
            <?=do_shortcode( '[wp_social_sharing]' );?>
	<?php // else: ?>
            <!--div style="color:red">Publish the post before sharing it!</div -->
	<?php // endif; ?>

