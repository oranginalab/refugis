

<div class="close"></div><?php 
$language_active = ICL_LANGUAGE_CODE;

if($language_active=='es') :
?>

<h2 class="section-title"><span> Memoria</span></h2>
<div class="text-block">
<p><span class="visible">Los refugios antiaéreos de Barcelona fueron construidos por la ciudadanía o por la administración con el objetivo de protegerse de los brutales bombardeos aéreos que sufrió la Ciudad durante la Guerra Civil</span><span>, forman parte de nuestro patrimonio y nuestra memoria colectiva. Se han convertido en símbolos de autoorganización popular, de resistencia y de lucha.  Por eso también,  su conservación y documentación han sido objeto de campañas de reivindicación vecinal.</span> </p>
<p><span>En este web interactivo impulsado por la Regiduría de Memoria Democrática y el Servicio de Arqueología del Ayuntamiento de Barcelona ponemos a disposición de la ciudadanía toda la información que disponemos actualmente de cada uno de los refugios documentados.</span></p>
<p><span>Encontrareis la investigación efectuada durante más de 20 años por diversos especialistas. Así mismo no consideramos que el trabajo esté acabado, al contrario, en esta web concentramos toda la información para ponerla al servicio de la ciudadanía, como ya hicimos hace años con la Carta Arqueológica de Barcelona; y también como pasa con la carta, será necesario una renovación constante, añadiendo todos los nuevos descubrimientos e investigaciones.</span></p>
<p><span>Es por eso que hacemos un llamamiento a todos los que estén interesados y dispongan de información y que la quieran compartir, que contacte con nosotros.</span></p>
<p><span>Entrad y navegad por el mapa. Seguro que descubriréis una nueva manera de observar y percibir nuestra Ciudad. Así mismo tenéis información sobre que se necesita hacer para documentar un refugio, como trabajan los arqueólogos  con la documentación, y sobre el contexto histórico  en cuál se construyeron estas estructuras confinadas, creando una ciudad subterránea para huir del horror de los bombardeos fascistas.</span></p>
<p>&nbsp;</p>
	<p<span><strong>Jordi Rabassa</strong><br/>
	Regidor de Memòria Democràtica</span></p>
</div>

<?php elseif($language_active=='en') : ?>

<h2 class="section-title"><span>  Memory</span></h2>
<div class="text-block">
	
	
<p><span class="visible">Barcelona's air raid shelters were built by citizens or the administration with the aim of protecting against the brutal air bombardment suffered by the city during the Civil War, </span><span>and are part of our heritage and collective memory, and have become symbols of popular self-organization, resistance and struggle. For these reasons the shelters have also been the subject of neighborhood claim campaigns for their conservation and documentation.</span> </p>
<p><span>In this interactive website promoted by the Department of Democratic Memory and the Archaeology Service of the Barcelona City Council we make available to the public all the information that we currently have of each of the documented shelters.</span> </p>
<p><span>You will find research carried out over 20 years by various specialists. However, we do not consider the work to be completed, on the contrary, with this website we concentrate all the information to put it at the service of citizens, as we did years ago with the Carta Arqueològica de Barcelona; and also as with the chart, it is necessary a constant renewal, adding all the new discoveries and searches.</span> </p>
<p><span>That is why we call on everyone who is interested and has information, and who wants to share it to contact us.</span> </p>
<p><span>Enter and surf the map. You will surely discover a new way of observing and perceiving our City. You also have information about what to do when documenting a shelter, how archaeologists work in their documentation, and about the historical context in which these confined structures had to be built, creating an underground city to flee the horror of fascist bombing.</span> </p>
<p>&nbsp;</p>
	<p<span><strong>Jordi Rabassa</strong><br/>
Regidor de Memòria Democràtica</span></p>
</div>
<?php else : ?>

<h2 class="section-title"><span> Memòria</span></h2>
<div class="text-block">
<p><span class="visible">Els refugis antiaeris de Barcelona van ser construïts per la ciutadania o per l’administració amb l’objectiu de protegir-se dels brutals bombardejos aeris que va patir la ciutat durant la Guerra Civil</span><span>, i formen part del nostre patrimoni i la nostra memòria col·lectiva, i han esdevingut símbols d’autoorganització popular, de resistència i de lluita. Per aquests motius també han estat objecte de campanyes de reivindicació veïnal de la seva conservació i documentació. </span></p>
<p><span>En aquest web interactiu impulsat per la Regidoria de Memòria Democràtica i el Servei d’Arqueologia de l’Ajuntament de Barcelona posem a disposició de la ciutadania tota la informació de la que disposem actualment de cadascun dels refugis documentats. </span></p>
<p><span>Hi trobareu la recerca efectuada durant més de 20 anys per diversos especialistes. Tanmateix, no considerem la feina estigui enllestida, al contrari, amb aquest web concentrem tota la informació per posar-la al servei de la ciutadania, tal com ja vam fer fa anys amb la Carta Arqueològica de Barcelona; i també com passa amb la carta, és necessària una renovació constant, afegint totes les noves descobertes i recerques.</span></p> 
<p><span>És per això que fem una crida a tothom que estigui interessat i que disposi d’informació, i que la vulgui compartir que contacti amb nosaltres.</span></p>
<p><span>Entreu i navegueu pel mapa. De ben segur que descobrireu una nova manera d’observar i percebre la nostra Ciutat.. Així mateix teniu informació sobre que cal fer en documentar un refugi, com treballen els arqueòlegs en la seva documentació, i sobre el context històric en que es van haver de construir aquestes estructures confinades, creant una ciutat subterrània per fugir de l’horror dels bombardejos feixistes.</span></p>

<p>&nbsp;</p>
<p><span><strong>Jordi Rabassa</strong><br/>
Regidor de Memòria Democràtica</span></p>
</div>


<? endif ?>