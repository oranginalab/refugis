<?php

/**
 * Redirect to WPML default language home if site URL has no language segment but "Utilitzar directori per a l'idioma predeterminat" option is checked.
 *
 * Set this file in WPML settings:  "Idiomes" / "Format de la URL d'idioma" / "Fitxer HTML – introdueixi un ruta: carpeta d'instal·lació de WordPress absoluta o relativa",
 * e.g. wp-content/themes/refugis-2020-v6/redirect_home_wpml.php
 */
wp_redirect( home_url() );
