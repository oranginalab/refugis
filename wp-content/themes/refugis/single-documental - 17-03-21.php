<?php

$isAjax = isset( $_SERVER['HTTP_X_REQUESTED_WITH'] ) && strtolower( $_SERVER['HTTP_X_REQUESTED_WITH'] ) == 'xmlhttprequest';
?>

<?php if ( $isAjax ): ?>

	<?php $terms = get_the_terms( $post->ID, 'categoria' ); ?>

    <div class="geo">

        <div class="block-one">
            <h1><?php _e( "<span>Barcelona</span><span>sota les</span><span>bombes</span>", "refugis" ); ?></h1>
        </div>
        <h2 class="section-title">
            <span><?php _e( "<span>Context</span> històric", "refugis" ); ?></span>
        </h2>
        <p class="subtitle-off"><?php foreach ( $terms as $term ) {
				echo $term->name;
			} ?></p>
        <p class="docuement-title"><?php the_title(); ?></p>

    </div>


    <header>

        <div class="general-image">

			<?php

			if ( get_field( 'imatge_capcalera' ) ) {
				$image = get_field( 'imatge_capcalera' );
				$size  = 'full';
				echo '<img src="' . wp_get_attachment_image( $image, $size ) . '">';
			}

			?>
           <?php if ( get_field( 'peu_imatge_capcalera' ) ) { ?>
            <span class="credits-imatge"><?php the_field("peu_imatge_capcalera"); ?></span>
            <?php } ?>
        </div>


        <div class="top-group">
            <div class="marges-header">
                <div>
                    <p class="subtitle"><?php foreach ( $terms as $term ) {
							echo $term->name;
						} ?></p>
                    <h3><span><?php the_title(); ?></span></h3>
                </div>
            </div>
        </div>

        <svg preserveAspectRatio="none" viewBox="0 0 100 100">
            <defs>
                <linearGradient id="gradient-masq-image" gradientTransform="rotate(90)">
                    <stop offset="0%" stop-color="rgba(0,0,0,0)"></stop>

                    <stop offset="50%" stop-color="rgba(0,0,0,0.1)"></stop>
                    <stop offset="90%" stop-color="rgba(0,0,0,0.2)"></stop>
                </linearGradient>
            </defs>
            <polygon fill="url(#gradient-masq-image)" points="0,0 100,0 100,100 0,100 "></polygon>
        </svg>


    </header>

    <div class="back-color">
        <div class="content">


            <div class="intro">
                <div><?php the_field( 'introducció' ); ?></div>
            </div>
            <div class="content-wp"><?php
				if ( have_posts() ):
					while ( have_posts() ) : the_post();
						the_content();
					endwhile;
				endif;
				?>
            </div>
			<?php $ID = get_the_ID(); ?>
			<?php
			if ( get_field( 'continguts', $ID ) ) { ?>
				<?php while ( the_repeater_field( 'continguts', $ID ) ) { ?>

                    <article class="block <?php the_sub_field( 'posicio' ); ?> "
					         <?php if ( get_sub_field( 'marge_superior' ) ) { ?>style="margin-top:<?php the_sub_field( 'marge_superior' ); ?>vh;" <?php } ?>>
						<?php if ( get_sub_field( 'titol_bloc' ) ) { ?>
                            <h4><?php the_sub_field( 'titol_bloc' ); ?></h4> <?php } ?>
						<?php the_sub_field( 'bloc' ); ?>
                    </article>

				<?php } ?>
			<?php } ?>

        </div>
        <footer>

			<?=do_shortcode( '[wp_social_sharing]' ); // ALEX ?>
        </footer>
    </div>

    </div>
	<?php /*
    <div class="next-doc">

		$next_post = get_next_post();
		if ( $next_post ) {
			$next_title = strip_tags( str_replace( '"', '', $next_post->post_title ) );
			echo '<a href="' . $next_post->post_name . '" title="' . $next_title . '" class=" "><span>seguir:</span>' . $next_title . '</a>';
		}


    </div> */ ?>
    <script>
      $(document).ready(function () {
        // Remove PHP warnings.
        $("footer .xdebug-error").remove();
        $("footer font").remove();

        // Hide un-styled sharing buttons from plugin.
        $("div.social-sharing").hide();

        // Capture styles buttons clicks and put them in action.
        $("li.twitter").click(function () {
          console.log('Twitter share clicked');
          $("div.social-sharing a.button-twitter").click();
        })
        $("li.facebook").click(function () {
          console.log('Facebook share clicked');
          $("div.social-sharing a.button-facebook").click();
        })
      });
    </script>

<?php else: ?>

<?php

global $post;

$type = $post->post_type;
$meta = get_post_meta( $post->ID );

// Here we can fetch more post data, e.g. thumbnail image, attachments, taxonomies, etc.

// Use real documentary photo when available.
$default_photo = get_template_directory_uri() . '/images/bg-bombes-01.jpg';
if ( get_field( 'imatge_capcalera' ) ) {
	$image = get_field( 'imatge_capcalera' );
	$photo = wp_get_attachment_image_src( $image, 'full' );
	if ( $photo ) {
		$photo = $photo[0];
	}
}
if ( empty( $photo ) ) {
	$photo = $default_photo;
}

// Sanitize description.
$description = wp_trim_words( strip_shortcodes( wp_filter_nohtml_kses( preg_replace( '/<!--(.*)-->/Uis', '', $post->post_content ) ) ), 50 );

$options              = get_option( 'rfg_settings', [] );
$twitter_site_meta    = isset( $options['rfg_twitter_site_meta'] ) ? $options['rfg_twitter_site_meta'] : '@rfg';
$twitter_creator_meta = isset( $options['rfg_twitter_creator_meta'] ) ? $options['rfg_twitter_creator_meta'] : '@rfg';

// *** Special case: we have to update post slug to decouple it from default permalink.
/* @link wp-content/themes/refugis-2020-v6/js/rfg-navigo.js:84 */
$lang     = defined( 'ICL_LANGUAGE_CODE' ) ? ICL_LANGUAGE_CODE . '-' : '';
$redirect = rtrim(home_url(),'/') . '/context-historic/' . $lang . $post->post_name;
?>
<!doctype html>
<html lang="<?php echo $lang = get_bloginfo( "language" ); ?>">
<head>

    <!--meta name="viewport"
          content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta charset="UTF-8"-->
    <title>Refugis. Servei Arqueologia</title>


    <!-- Twitter Share -->
    <meta name="twitter:card" content="summary_large_image">
    <meta name="twitter:site" content="<?=$twitter_site_meta?>">
    <meta name="twitter:creator" content="<?=$twitter_creator_meta?>">
    <meta name="twitter:title" content="<?=$post->post_title?>">
    <meta name="twitter:image" content="<?=$photo?>">


    <!-- Facebook/LinkedIn Share -->
    <meta property="og:url" content="<?=get_post_permalink( $post->ID )?>"/>
    <meta property="og:type" content="website"/>
    <meta property="og:title" content="<?=$post->post_title?>"/>
    <meta property="og:description" content="<?=$description?>"/>
    <meta property="og:image" content="<?=$photo?>"/>
    <meta property="og:image:secure_url" content="<?=$photo?>"/>

    <!--link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bulma@0.8.0/css/bulma.min.css">
    <script defer src="https://use.fontawesome.com/releases/v5.3.1/js/all.js"></script-->
</head>
<body>
<!--section class="section has-text-centered">
    <div class="container">
        <h1 class="title">
            RFG Shelters
        </h1>
        <p class="subtitle">
            <span class="icon"><i class="fas fa-share"></i></span>
            You will be redirected to documentary page in <span id="timer"></span> seconds...
        </p>
        <div class="columns">
            <div class="column is-half is-offset-one-quarter">
                <figure class="image is-256x256">
                    <img src="<? /*=$photo*/ ?>" alt="rfg-shelter">
                </figure>
            </div>
        </div>
    </div>
</section-->
<script>
  // Redirect to site right away.
  document.location = '<?=$redirect?>';
  //var timeout = 5, current;
  //document.getElementById('timer').innerText = timeout.toString();
  //window.setInterval(function () {
  //  current = document.getElementById('timer').innerText;
  //  current--;
  //  document.getElementById('timer').innerText = current.toString();
  //  if (current <= 1) {
  //    document.location = '<?//=$redirect?>';
  //  }
  //}, 1000);
</script>
</body>

<?php endif; ?>
