<?php

global $post;

$type = $post->post_type;
$meta = get_post_meta( $post->ID );

$shelter_api_id = $meta['api_id'][0];

// Here we can fetch more post data, e.g. thumbnail image, attachments, taxonomies, etc.

$redirect = home_url() . '?s_id=' . $shelter_api_id;

// Get api data to enrich metas.
$api_data = rfg_single_shelter_get_from_api( $shelter_api_id );



//  TRES IDIOMES TRES IMATGES : if($lang=="es"){ socials_es.jpg } elseif($lang=="en-US"){ socials_en.jpg } else { socials_ca.jpg } ;


// Use real shelter photos when available.
$default_photo = get_template_directory_uri() . '/images/img-refugis/socials.jpg';

// Site title per language
$site_title = 'La ciutat dels refugis';
$default_photo = get_template_directory_uri() . '/images/img-refugis/socials_ca.jpg';
if (ICL_LANGUAGE_CODE) {
    switch (ICL_LANGUAGE_CODE) {
        case 'es':
            $site_title = 'La ciudad de los refugios';
            $default_photo = get_template_directory_uri() . '/images/img-refugis/socials_es.jpg';
            break;

        case 'en':
            $site_title = 'The city of shelters';
            $default_photo = get_template_directory_uri() . '/images/img-refugis/socials_en.jpg';
            break;

        case 'ca':
        default:
            $site_title = 'La ciutat dels refugis';
            $default_photo = get_template_directory_uri() . '/images/img-refugis/socials_ca.jpg';
    }
}


if ( $api_data['success'] && $api_data['json']->photos ) {
	// TODO: we may need to refactor when API team fixes photos URLs.
	// This is a dirty workaround. Actual API photos make a redirect to another resource, so photo URI
	// fails when set as og:image meta.
	// Here we get target URI making a get request without redirects.
	$api_photo = $api_data['json']->photos[0]->Url;
	$response  = wp_remote_get( 'http://' . $api_photo, [
		'redirection' => 0,
	] );
	$photo     = wp_remote_retrieve_header( $response, 'location' );
}
if ( empty( $photo ) ) {
	$photo = $default_photo;
}

// Sanitize description.
// $description = wp_trim_words( strip_shortcodes( wp_filter_nohtml_kses( preg_replace( '/<!--(.*)-->/Uis', '', $post->post_content ) ) ), 50 );
$description = '';

if ($api_data['success']){
	$shelter_name = $api_data['json']->name;
	if ( (is_object($api_data['json']->data->$shelter_name)) &&
	     (is_object($api_data['json']->data->$shelter_name->DESCRIPCIÓ)) &&
	     (is_object($api_data['json']->data->$shelter_name->DESCRIPCIÓ->Descripció)) ) {
		$description = wp_trim_words( strip_shortcodes( wp_filter_nohtml_kses($api_data['json']->data->$shelter_name->DESCRIPCIÓ->Descripció->value) ), 50);
	}
}

$options              = get_option( 'rfg_settings', [] );
$twitter_site_meta    = isset( $options['rfg_twitter_site_meta'] ) ? $options['rfg_twitter_site_meta'] : '@rfg';
$twitter_creator_meta = isset( $options['rfg_twitter_creator_meta'] ) ? $options['rfg_twitter_creator_meta'] : '@rfg';
?>
    <!doctype html>
<html lang="<?php echo $lang = get_bloginfo( "language" ); ?>">
<head>

    <!--meta name="viewport"
          content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta charset="UTF-8"-->
    <title>Refugis. Servei Arqueologia</title>


    <!-- Twitter Share -->
    <meta name="twitter:card" content="summary_large_image">
    <meta name="twitter:site" content="<?=$twitter_site_meta?>">
    <meta name="twitter:creator" content="<?=$twitter_creator_meta?>">
    <meta name="twitter:title" content="<?php echo $site_title .' - '. $post->post_title?>">
    <meta name="twitter:image" content="<?=$photo?>">


    <!-- Facebook/LinkedIn Share -->
    <meta property="og:url" content="<?=get_post_permalink( $post->ID )?>"/>
    <meta property="og:type" content="website"/>
    <meta property="og:title" content="<?php echo $site_title .' - '. $post->post_title?>"/>
    <meta property="og:description" content="<?=$description?>"/>
    <meta property="og:image" content="<?=$photo?>"/>
    <meta property="og:image:secure_url" content="<?=$photo?>"/>

    <!--link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bulma@0.8.0/css/bulma.min.css">
    <script defer src="https://use.fontawesome.com/releases/v5.3.1/js/all.js"></script-->
</head>
<body>
<!--section class="section has-text-centered">
    <div class="container">
        <h1 class="title">
            RFG Shelters
        </h1>
        <p class="subtitle">
            <span class="icon"><i class="fas fa-share"></i></span>
            You will be redirected to shelter page in <span id="timer"></span> seconds...
        </p>
        <div class="columns">
            <div class="column is-half is-offset-one-quarter">
                <figure class="image is-256x256">
                    <img src="<?/*=$photo*/?>" alt="rfg-shelter">
                </figure>
            </div>
        </div>
    </div>
</section-->
<script>
  // Redirect to site right away.
  document.location = '<?=$redirect?>';
  //var timeout = 5, current;
  //document.getElementById('timer').innerText = timeout.toString();
  //window.setInterval(function () {
  //  current = document.getElementById('timer').innerText;
  //  current--;
  //  document.getElementById('timer').innerText = current.toString();
  //  if (current <= 1) {
  //    document.location = '<?//=$redirect?>';
  //  }
  //}, 1000);
</script>
</body>
<?php

function rfg_single_shelter_get_from_api( $id ) {
	$url = RFG_SHELTERS_API_BASE_URL . 'refugi/' . $id;

	$args = [
		'timeout' => 20,
	];

	// Do remote get
	$data     = [
		'success' => true,
	];
	$code     = 0;
	$response = wp_remote_get( $url, $args );

	if ( ! is_wp_error( $response ) && ( $code = wp_remote_retrieve_response_code( $response ) ) == 200 ) {
		$data['json'] = json_decode( wp_remote_retrieve_body( $response ) );
	} else {
		$data['success'] = false;
		$data['error']   = is_wp_error( $response ) ? $response->get_error_message() : $code;
	}

	return $data;
}

